import { ETimestamp, IName, IUnique } from "data/shared";

// RESPONSE ENTITIES

export interface EDeviceType extends IUnique, IName {}

export interface EDeviceModel extends IName {
  device_type: EDeviceType;
  configuration: {};
}

export interface EDevice extends IUnique, ETimestamp, EDeviceModel {}

export type EDevices = EDevice[];

export interface EDeviceSecret {
  device_secret: string;
  secret_algorithm: string;
}

// REQUEST ENTITIES

export interface EGetDevice extends IUnique {}
export interface ERemoveDevice extends IUnique {}
export interface EPostDevice extends IName {
  device_type_id: number;
}

export interface EGetDeviceSecret {
  device_id: number;
}
