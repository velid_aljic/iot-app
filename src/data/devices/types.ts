import { IName, ITimestamp, IUnique } from "data/shared";

// RESPONSE INTERFACES

export interface IDeviceType extends IUnique, IName {}

export interface IDeviceModel extends IName {
  deviceType: IDeviceType;
  configuration: {};
}

export interface IDevice extends IUnique, ITimestamp, IDeviceModel {}

export type IDevices = IDevice[];

export interface IDeviceSecret {
  deviceSecret: string;
  secretAlgorithm: string;
}

// REQUEST INTERFACES

export interface IGetDevice extends IUnique {}
export interface IRemoveDevice extends IUnique {}
export interface IPostDevice extends IName {
  deviceTypeId: number;
}

export interface IGetDeviceSecret {
  deviceId: number;
}
