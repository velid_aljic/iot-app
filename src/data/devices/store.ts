import moment from "moment";

import { asyncActionCreator, asyncDispatcherAdapter, concatToList, IAction, removeFromList } from "utils/redux";

import { EDevice, EDevices, EDeviceSecret } from "./entities";
import { mapDeviceSecretToInterface, mapDevicesToInterface, mapDeviceToInterface } from "./mappers";
import { getDevice, getDevices, getDeviceSecret, postDevice, removeDevice } from "./service";
import { IDevice, IDevices, IDeviceSecret, IGetDevice, IGetDeviceSecret, IPostDevice, IRemoveDevice } from "./types";

/**
 *
 *
 * Actions
 * <response, params>
 */
export const fetchDevicesAction = asyncActionCreator<EDevices, undefined>("FETCH_DEVICES");
export const fetchDeviceAction = asyncActionCreator<EDevice, IGetDevice>("FETCH_DEVICE");
export const deleteDeviceAction = asyncActionCreator<EDevice, IRemoveDevice>("DELETE_DEVICE");
export const createDeviceAction = asyncActionCreator<EDevice, IPostDevice>("CREATE_DEVICE");

export const fetchDevicesSecretAction = asyncActionCreator<EDeviceSecret, IGetDeviceSecret>("FETCH_DEVICE_SECRET");

/**
 *
 *
 * Dispatchers
 * <response, params>
 */
export const fetchDevices = asyncDispatcherAdapter<EDevices, void>(fetchDevicesAction, getDevices);
export const fetchDevice = asyncDispatcherAdapter<EDevice, IGetDevice>(fetchDeviceAction, getDevice);
export const deleteDevice = asyncDispatcherAdapter<EDevice, IRemoveDevice>(deleteDeviceAction, removeDevice);
export const createDevice = asyncDispatcherAdapter<EDevice, IPostDevice>(createDeviceAction, postDevice);

export const fetchDeviceSecret = asyncDispatcherAdapter<EDeviceSecret, IGetDeviceSecret>(
  fetchDevicesSecretAction,
  getDeviceSecret,
);

/**
 *
 *
 * Store
 * State interface & initialState
 */
export interface IDevicesStore {
  isWorking: boolean;
  list: IDevices | [];
  item: IDevice;

  secret: IDeviceSecret;
  isFetchingSecret: boolean;
}

const initialState: IDevicesStore = {
  isWorking: false,
  list: [],
  item: {
    configuration: {},
    createdAt: moment(),
    deviceType: {
      id: 0,
      name: "",
    },
    id: 0,
    modifiedAt: moment(),
    name: "",
  },
  secret: {
    deviceSecret: "",
    secretAlgorithm: "",
  },
  isFetchingSecret: false,
};

/**
 *
 *
 * Action types
 */
const FETCH_DEVICES_STARTED = "FETCH_DEVICES_STARTED";
const FETCH_DEVICES_SUCCESS = "FETCH_DEVICES_SUCCESS";
const FETCH_DEVICES_FAILURE = "FETCH_DEVICES_FAILURE";

const FETCH_DEVICE_STARTED = "FETCH_DEVICE_STARTED";
const FETCH_DEVICE_SUCCESS = "FETCH_DEVICE_SUCCESS";
const FETCH_DEVICE_FAILURE = "FETCH_DEVICE_FAILURE";

const CREATE_DEVICE_STARTED = "CREATE_DEVICE_STARTED";
const CREATE_DEVICE_SUCCESS = "CREATE_DEVICE_SUCCESS";
const CREATE_DEVICE_FAILURE = "CREATE_DEVICE_FAILURE";

const DELETE_DEVICE_STARTED = "DELETE_DEVICE_STARTED";
const DELETE_DEVICE_SUCCESS = "DELETE_DEVICE_SUCCESS";
const DELETE_DEVICE_FAILURE = "DELETE_DEVICE_FAILURE";

const FETCH_DEVICE_SECRET_STARTED = "FETCH_DEVICE_SECRET_STARTED";
const FETCH_DEVICE_SECRET_SUCCESS = "FETCH_DEVICE_SECRET_SUCCESS";
const FETCH_DEVICE_SECRET_FALURE = "FETCH_DEVICE_SECRET_FALURE";

/**
 *
 *
 * REDUCER
 */

export const reducer = (state: IDevicesStore = initialState, action: IAction<any, any>) => {
  switch (action.type) {
    // FETCH_DEVICES
    case FETCH_DEVICES_STARTED:
      return { ...state, isWorking: true };
    case FETCH_DEVICES_SUCCESS:
      return { ...state, isWorking: false, list: mapDevicesToInterface(action.payload) };
    case FETCH_DEVICES_FAILURE:
      return { ...state, isWorking: false, list: [] };

    // FETCH_DEVICE
    case FETCH_DEVICE_STARTED:
      return { ...state, isWorking: true };
    case FETCH_DEVICE_SUCCESS:
      return { ...state, isWorking: false, item: mapDeviceToInterface(action.payload) };
    case FETCH_DEVICE_FAILURE:
      return { ...state, isWorking: false, item: {} };

    // CREATE_DEVICE
    case CREATE_DEVICE_STARTED:
      return { ...state, isWorking: true };
    case CREATE_DEVICE_SUCCESS:
      return {
        ...state,
        list: concatToList<IDevice>(state.list, mapDeviceToInterface(action.payload)),
        isWorking: false,
      };
    case CREATE_DEVICE_FAILURE:
      return { ...state, isWorking: false };

    // DELETE_DEVICE
    case DELETE_DEVICE_STARTED:
      return { ...state, isWorking: true };
    case DELETE_DEVICE_SUCCESS:
      return {
        ...state,
        list: removeFromList<IDevice>(state.list, action.meta),
        isWorking: false,
      };
    case DELETE_DEVICE_FAILURE:
      return { ...state, isWorking: false };

    // FETCH_DEVICE_SECRET
    case FETCH_DEVICE_SECRET_STARTED:
      return {
        ...state,
        isFetchingSecret: true,
        secret: {
          deviceSecret: "",
          secretAlgorithm: "",
        },
      };
    case FETCH_DEVICE_SECRET_SUCCESS:
      return {
        ...state,
        secret: mapDeviceSecretToInterface(action.payload),
        isFetchingSecret: false,
      };
    case FETCH_DEVICE_SECRET_FALURE:
      return { ...state, isFetchingSecret: false };

    // DEFAULT
    default:
      return state;
  }
};
