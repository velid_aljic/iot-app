// ICONS

const RANDOM = "question";
const THERMOMETER = "thermometer three quarters";
const MOTION = "move";
const ALTITUDE = "level up";
const HUMIDITY = "theme";
const GYROSCOPE = "periscope";

// DEVICE TYPES

export const deviceTypes = [
  { text: "Thermometer", icon: THERMOMETER, value: 2 },
  { text: "Motion", icon: MOTION, value: 6 },
  { text: "Altitude", icon: ALTITUDE, value: 4 },
  { text: "Humidity", icon: HUMIDITY, value: 3 },
  { text: "Gyroscope", icon: GYROSCOPE, value: 5 },
  { text: "Random", icon: RANDOM, value: 1 },
];

// TYPE <=> ICON

export const resolveIcon = (type: number) => {
  switch (type) {
    case 1:
      return RANDOM;
    case 2:
      return THERMOMETER;
    case 3:
      return HUMIDITY;
    case 4:
      return ALTITUDE;
    case 5:
      return GYROSCOPE;
    case 6:
      return MOTION;

    default:
      return RANDOM;
  }
};
