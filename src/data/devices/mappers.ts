import { EDevice, EDevices, EDeviceSecret, EGetDeviceSecret, EPostDevice } from "./entities";
import { IDevice, IDevices, IDeviceSecret, IGetDeviceSecret, IPostDevice } from "./types";

export const mapDeviceToInterface = (device: EDevice): IDevice => {
  const { device_type, created_at, modified_at, ...rest } = device;
  return {
    deviceType: device_type,
    createdAt: created_at,
    modifiedAt: modified_at,
    ...rest,
  };
};

export const mapDeviceSecretToInterface = (secret: EDeviceSecret): IDeviceSecret => {
  const { device_secret, secret_algorithm } = secret;

  return {
    deviceSecret: device_secret,
    secretAlgorithm: secret_algorithm,
  };
};

export const mapDevicesToInterface = (devices: EDevices): IDevices => {
  return devices.map(mapDeviceToInterface);
};

export const mapPostDeviceToEntity = (params: IPostDevice): EPostDevice => {
  const { deviceTypeId, ...rest } = params;
  return {
    device_type_id: deviceTypeId,
    ...rest,
  };
};

export const mapGetDeviceSecretToEntity = (params: IGetDeviceSecret): EGetDeviceSecret => {
  return {
    device_id: params.deviceId,
  };
};
