import { get, IResponse, post, remove } from "utils/http";
import { compose } from "utils/url";

import { EDevice, EDevices, EDeviceSecret } from "./entities";
import { mapGetDeviceSecretToEntity, mapPostDeviceToEntity } from "./mappers";
import { IGetDevice, IGetDeviceSecret, IPostDevice, IRemoveDevice } from "./types";

const API = {
  list: `/api/v1/devices`,
  item: `/api/v1/devices/{id}`,
  secret: `/api/v1/devices/{device_id}/secret`,
};

export const getDevices = () => {
  const url = compose(API.list);
  return get<IResponse<EDevices>>(url);
};

export const getDevice = (params: IGetDevice) => {
  const url = compose(
    API.item,
    params,
  );
  return get<IResponse<EDevice>>(url);
};

export const postDevice = (data: IPostDevice) => {
  const url = compose(API.list);
  return post<IResponse<EDevice>>(url, mapPostDeviceToEntity(data));
};

export const removeDevice = (params: IRemoveDevice) => {
  const url = compose(
    API.item,
    params,
  );
  return remove<IResponse<EDevice>>(url);
};

export const getDeviceSecret = (data: IGetDeviceSecret) => {
  const paramsE = mapGetDeviceSecretToEntity(data);
  const url = compose(
    API.secret,
    paramsE,
  );
  return get<IResponse<EDeviceSecret>>(url);
};
