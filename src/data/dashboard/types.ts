import { IRecordings } from "data/recordings";
import { IFilter, IName, IPosition, IStatus, ITimestamp, IUnique } from "data/shared";

import { IDashboard } from "./types";

// RESPONSE INTERFACES

export interface IWidgetBase extends IName, IPosition {
  chartType: string;
  deviceId: number;
  filters: IFilter;
}

export interface IWidget extends IStatus, IUnique, ITimestamp, IWidgetBase {
  data: IRecordings;
}

export interface IDashboard extends IUnique, IName, ITimestamp {
  widgets: IWidget[];
}

export type IDashboards = IDashboard[];

// REQUEST INTERFACES

// dashboard

export interface IPostDashboard extends IName {}
export interface IGetDashboard extends IUnique {}
export interface IRemoveDashboard extends IUnique {}

// widgets

export interface IPostWidget extends IWidgetBase {
  dashboardId: number;
}

export interface IPatchWidget extends Partial<IWidgetBase> {
  dashboardId: number;
  widgetId: number;
}

export interface IPutWidget extends IPatchWidget {}

export interface IRemoveWidget {
  dashboardId: number;
  widgetId: number;
}
