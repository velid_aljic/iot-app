import { EFilter, ETimestamp, IName, IPosition, IUnique } from "../shared";

import { EDashboard } from "./entities";

// RESPONSE ENTITIES

export interface EWidgetBase extends IName, IPosition {
  chart_type: string;
  device_id: number;
  filters: EFilter;
}

export interface EWidget extends EWidgetBase, ETimestamp, IUnique {}

export interface EDashboard extends IName, IUnique, ETimestamp {
  widgets: EWidget[];
}

export type EDashboards = EDashboard[];

// REQUEST ENTITIES

// dashboard

export interface EPostDashboard extends IName {}
export interface EGetDashboard extends IUnique {}
export interface ERemoveDashboard extends IUnique {}

// widgets

export interface EPostWidget extends EWidgetBase {
  dashboard_id: number;
}

export interface EPatchWidget extends Partial<EWidgetBase> {
  dashboard_id: number;
  widget_id: number;
}

export interface EPutWidget extends EPatchWidget {}

export interface ERemoveWidget {
  dashboard_id: number;
  widget_id: number;
}
