import moment from "moment";

import { asyncActionCreator, asyncDispatcherAdapter, concatToList, IAction, removeFromList } from "utils/redux";

import { EDashboard, EDashboards, EWidget } from "./entities";
import { mapDashboardsToInterface, mapDashboardToInterface } from "./mappers";
import {
  getDashboard,
  getDashboards,
  patchWidgetService,
  postDashboard,
  postWidget,
  removeDashboard,
  removeWidget,
} from "./service";
import {
  IDashboard,
  IDashboards,
  IGetDashboard,
  IPatchWidget,
  IPostDashboard,
  IPostWidget,
  IRemoveDashboard,
  IRemoveWidget,
  IWidget,
} from "./types";
import { attachWidget, ejectWidget, freshenRecordings, freshenWidget, propelWidget, terminateWidget } from "./utils";

/**
 *
 *
 * Actions
 */

// DASHBOARDS

export const fetchDashboardsAction = asyncActionCreator<EDashboards, undefined>("FETCH_DASHBOARDS");
export const fetchDashboardAction = asyncActionCreator<EDashboard, IGetDashboard>("FETCH_DASHBOARD");
export const deleteDashboardAction = asyncActionCreator<EDashboard, IRemoveDashboard>("DELETE_DASHBOARD");
export const createDashboardAction = asyncActionCreator<EDashboard, IPostDashboard>("CREATE_DASHBOARD");

// WIDGETS

export const createWidgetAction = asyncActionCreator<EWidget, IPostWidget>("CREATE_WIDGET");
export const updateWidgetAction = asyncActionCreator<EWidget, IPatchWidget>("UPDATE_WIDGET");
export const patchWidgetAction = asyncActionCreator<EWidget, IPatchWidget>("PATCH_WIDGET");
export const deleteWidgetAction = asyncActionCreator<EWidget, IRemoveWidget>("DELETE_WIDGET");

/**
 *
 *
 * Dispatchers
 */

// DASHBOARDS

export const fetchDashboards = asyncDispatcherAdapter<EDashboards, void>(fetchDashboardsAction, getDashboards);
export const fetchDashboard = asyncDispatcherAdapter<EDashboard, IGetDashboard>(fetchDashboardAction, getDashboard);
export const deleteDashboard = asyncDispatcherAdapter<EDashboard, IRemoveDashboard>(
  deleteDashboardAction,
  removeDashboard,
);
export const createDashboard = asyncDispatcherAdapter<EDashboard, IPostDashboard>(createDashboardAction, postDashboard);

// WIDGETS

export const createWidget = asyncDispatcherAdapter<EWidget, IPostWidget>(createWidgetAction, postWidget);
export const updateWidget = asyncDispatcherAdapter<EWidget, IPatchWidget>(updateWidgetAction, patchWidgetService);
export const patchWidget = asyncDispatcherAdapter<EWidget, IPatchWidget>(patchWidgetAction, patchWidgetService);
export const deleteWidget = asyncDispatcherAdapter<EWidget, IRemoveWidget>(deleteWidgetAction, removeWidget);

/**
 *
 *
 * Store
 * State interface & initialState
 */

export interface IDashboardsStore {
  isWorking: boolean;
  isCreatingWidget: boolean;
  list: IDashboards | [];
  item: IDashboard;
}

const initialState: IDashboardsStore = {
  isWorking: false,
  isCreatingWidget: false,
  list: [],
  item: {
    createdAt: moment(),
    id: 0,
    modifiedAt: moment(),
    name: "",
    widgets: [],
  },
};

/**
 *
 *
 * Action types
 */

// DASHBOARDS

const FETCH_DASHBOARDS_STARTED = "FETCH_DASHBOARDS_STARTED";
const FETCH_DASHBOARDS_SUCCESS = "FETCH_DASHBOARDS_SUCCESS";
const FETCH_DASHBOARDS_FAILURE = "FETCH_DASHBOARDS_FAILURE";

const FETCH_DASHBOARD_STARTED = "FETCH_DASHBOARD_STARTED";
const FETCH_DASHBOARD_SUCCESS = "FETCH_DASHBOARD_SUCCESS";
const FETCH_DASHBOARD_FAILURE = "FETCH_DASHBOARD_FAILURE";

const CREATE_DASHBOARD_STARTED = "CREATE_DASHBOARD_STARTED";
const CREATE_DASHBOARD_SUCCESS = "CREATE_DASHBOARD_SUCCESS";
const CREATE_DASHBOARD_FAILURE = "CREATE_DASHBOARD_FAILURE";

const DELETE_DASHBOARD_STARTED = "DELETE_DASHBOARD_STARTED";
const DELETE_DASHBOARD_SUCCESS = "DELETE_DASHBOARD_SUCCESS";
const DELETE_DASHBOARD_FAILURE = "DELETE_DASHBOARD_FAILURE";

// WIDGETS

const CREATE_WIDGET_STARTED = "CREATE_WIDGET_STARTED";
const CREATE_WIDGET_SUCCESS = "CREATE_WIDGET_SUCCESS";
const CREATE_WIDGET_FAILURE = "CREATE_WIDGET_FAILURE";

const UPDATE_WIDGET_STARTED = "UPDATE_WIDGET_STARTED";
const UPDATE_WIDGET_SUCCESS = "UPDATE_WIDGET_SUCCESS";
const UPDATE_WIDGET_FAILURE = "UPDATE_WIDGET_FAILURE";

const DELETE_WIDGET_STARTED = "DELETE_WIDGET_STARTED";
const DELETE_WIDGET_SUCCESS = "DELETE_WIDGET_SUCCESS";
const DELETE_WIDGET_FAILURE = "DELETE_WIDGET_FAILURE";

// RECORDINGS

const FETCH_RECORDINGS_STARTED = "FETCH_RECORDINGS_STARTED";
const FETCH_RECORDINGS_SUCCESS = "FETCH_RECORDINGS_SUCCESS";
const FETCH_RECORDINGS_FAILURE = "FETCH_RECORDINGS_FAILURE";

/**
 *
 *
 * Reducer
 */

export const reducer = (state: IDashboardsStore = initialState, action: IAction<any, any>) => {
  switch (action.type) {
    // DASHBOARDS

    /**
     *
     *
     * FETCH_DASHBOARDS
     */
    case FETCH_DASHBOARDS_STARTED:
      return { ...state, isWorking: true };
    case FETCH_DASHBOARDS_SUCCESS:
      return { ...state, list: mapDashboardsToInterface(action.payload), item: {}, isWorking: false };
    case FETCH_DASHBOARDS_FAILURE:
      return { ...state, isWorking: false, list: [] };

    /**
     *
     *
     * FETCH_DASHBOARD
     */
    case FETCH_DASHBOARD_STARTED:
      return { ...state, isWorking: true };
    case FETCH_DASHBOARD_SUCCESS:
      return {
        ...state,
        item: mapDashboardToInterface(action.payload),
        isWorking: false,
      };
    case FETCH_DASHBOARD_FAILURE:
      return { ...state, item: {}, isWorking: false };

    /**
     *
     *
     * CREATE_DASHBOARD
     */
    case CREATE_DASHBOARD_STARTED:
      return { ...state, isWorking: true };
    case CREATE_DASHBOARD_SUCCESS:
      return {
        ...state,
        list: concatToList<IDashboard>(state.list, mapDashboardToInterface(action.payload)),
        isWorking: false,
      };
    case CREATE_DASHBOARD_FAILURE:
      return { ...state, isWorking: false };

    /**
     *
     *
     * DELETE_DASHBOARD
     */
    case DELETE_DASHBOARD_STARTED:
      return { ...state, isWorking: true };
    case DELETE_DASHBOARD_SUCCESS:
      return {
        ...state,
        list: removeFromList<IDashboard>(state.list, action.meta),
        isWorking: false,
      };
    case DELETE_DASHBOARD_FAILURE:
      return { ...state, isWorking: false };

    // WIDGETS

    /**
     *
     *
     * CREATE_WIDGET
     */
    case CREATE_WIDGET_STARTED:
      return { ...state, isCreatingWidget: true };
    case CREATE_WIDGET_SUCCESS:
      return {
        ...state,
        item: {
          ...state.item,
          widgets: concatToList<IWidget>(state.item.widgets, attachWidget(action.payload)),
        },
        isCreatingWidget: false,
      };
    case CREATE_WIDGET_FAILURE:
      return { ...state, isCreatingWidget: false };

    /**
     *
     *
     * UPDATE_WIDGET
     */
    case UPDATE_WIDGET_STARTED:
      return {
        ...state,
        item: { ...state.item, widgets: propelWidget(state.item.widgets, action.meta) },
      };
    case UPDATE_WIDGET_SUCCESS:
      return {
        ...state,
        item: { ...state.item, widgets: freshenWidget(state.item.widgets, action) },
      };
    case UPDATE_WIDGET_FAILURE:
      return { ...state, item: { ...state.item, widgets: terminateWidget(state.item.widgets, action.meta) } };

    /**
     *
     *
     * DELETE_WIDGET
     */
    case DELETE_WIDGET_STARTED:
      return {
        ...state,
        item: { ...state.item, widgets: propelWidget(state.item.widgets, action.meta) },
      };
    case DELETE_WIDGET_SUCCESS:
      return {
        ...state,
        item: { ...state.item, widgets: ejectWidget(state.item.widgets, action.meta) },
      };
    case DELETE_WIDGET_FAILURE:
      return { ...state, item: { ...state.item, widgets: terminateWidget(state.item.widgets, action.meta) } };

    // RECORDINGS

    /**
     *
     *
     * FETCH_RECORDINGS
     */
    case FETCH_RECORDINGS_STARTED:
      return {
        ...state,
        item: { ...state.item, widgets: propelWidget(state.item.widgets, action.meta) },
      };
    case FETCH_RECORDINGS_SUCCESS:
      return {
        ...state,
        item: { ...state.item, widgets: freshenRecordings(state.item.widgets, action) },
      };
    case FETCH_RECORDINGS_FAILURE:
      return { ...state, item: { ...state.item, widgets: terminateWidget(state.item.widgets, action.meta) } };

    // DEFAULT

    default:
      return state;
  }
};
