import { get, IResponse, patch, post, put, remove } from "utils/http";
import { compose } from "utils/url";

import { EDashboard, EDashboards, EWidget } from "./entities";
import {
  mapPatchWidgetToEntity,
  mapPostWidgetToEntity,
  mapPutWidgetToEntity,
  mapRemoveWidgetToEntity,
} from "./mappers";
import {
  IGetDashboard,
  IPatchWidget,
  IPostDashboard,
  IPostWidget,
  IPutWidget,
  IRemoveDashboard,
  IRemoveWidget,
} from "./types";

const dashboardAPI = {
  list: `/api/v1/dashboards`,
  item: `/api/v1/dashboards/{id}`,
};

const widgetsAPI = {
  list: `/api/v1/dashboards/{dashboard_id}/widgets`,
  item: `/api/v1/dashboards/{dashboard_id}/widgets/{widget_id}`,
};

// DASHBOARD

// get dashboards
export const getDashboards = () => {
  const url = compose(dashboardAPI.list);
  return get<IResponse<EDashboards>>(url);
};

// get dashboard
export const getDashboard = (params: IGetDashboard) => {
  // doesen't require mapping
  const url = compose(
    dashboardAPI.item,
    params,
  );
  return get<IResponse<EDashboard>>(url);
};

// post dashboard
export const postDashboard = (data: IPostDashboard) => {
  const dataE = Object.assign(data, { dashboard_data: {} });

  const url = compose(dashboardAPI.list);
  return post<IResponse<EDashboard>>(url, dataE);
};

// remove dashboard
export const removeDashboard = (params: IRemoveDashboard) => {
  // doesen't require mapping
  const url = compose(
    dashboardAPI.item,
    params,
  );
  return remove<IResponse<EDashboard>>(url);
};

// WIDGETS

// post widget
export const postWidget = (params: IPostWidget) => {
  const paramsE = mapPostWidgetToEntity(params);

  const url = compose(
    widgetsAPI.list,
    paramsE,
  );
  return post<IResponse<EWidget>>(url, paramsE);
};

// put widget
export const putWidget = (params: IPutWidget) => {
  const paramsE = mapPutWidgetToEntity(params);

  const url = compose(
    widgetsAPI.item,
    paramsE,
  );
  return put<IResponse<EWidget>>(url, paramsE);
};

// patch widget
export const patchWidgetService = (params: IPatchWidget) => {
  const paramsE = mapPatchWidgetToEntity(params);

  const url = compose(
    widgetsAPI.item,
    paramsE,
  );
  return patch<IResponse<EWidget>>(url, paramsE);
};

// remove widget
export const removeWidget = (params: IRemoveWidget) => {
  const url = compose(
    widgetsAPI.item,
    mapRemoveWidgetToEntity(params),
  );
  return remove<IResponse<EWidget>>(url);
};
