import { IAction } from "utils/redux";
import { mapRecordingsToInterface } from "./../recordings/mappers";

import { ERecordings, IGetRecordings } from "../recordings";

import { EWidget } from "./entities";
import { mapWidgetToInterface } from "./mappers";
import { IPatchWidget, IRemoveWidget, IWidget } from "./types";

// ATTACH WIDGET
export const attachWidget = (widget: EWidget): IWidget => ({
  ...mapWidgetToInterface(widget),
  isDataStale: true,
});

// PROPEL WIDGET
export const propelWidget = (widgets: IWidget[], meta: IPatchWidget | IGetRecordings): IWidget[] => {
  return widgets.map((widget: IWidget) => {
    return widget.id === meta.widgetId ? { ...widget, isWorking: true } : widget;
  });
};

// FRESHEN WIDGET
export const freshenWidget = (widgets: IWidget[], action: IAction<EWidget, IPatchWidget>): IWidget[] => {
  return widgets.map((widget: IWidget) => {
    return widget.id === (action.meta as IPatchWidget).widgetId
      ? {
          ...widget,
          ...mapWidgetToInterface(action.payload as EWidget),
          isWorking: false,
          isDataStale: true,
          isFailure: false,
        }
      : widget;
  });
};

// EJECT WIDGET
export const ejectWidget = (widgets: IWidget[], meta: IRemoveWidget): IWidget[] => {
  return widgets.filter((widget: IWidget) => {
    return widget.id !== meta.widgetId;
  });
};

// TERMINATE WIDGET
export const terminateWidget = (widgets: IWidget[], meta: IPatchWidget | IGetRecordings): IWidget[] => {
  return widgets.map((widget: IWidget) => {
    return widget.id === meta.widgetId ? { ...widget, isWorking: false, isFailure: true } : widget;
  });
};

// FRESHEN RECORDINGS
export const freshenRecordings = (widgets: IWidget[], action: IAction<ERecordings, IGetRecordings>): IWidget[] => {
  return !!widgets
    ? widgets.map((widget: IWidget) => {
        return widget.id === (action.meta as IGetRecordings).widgetId
          ? {
              ...widget,
              isWorking: false,
              isDataStale: false,
              isFailure: false,
              data: mapRecordingsToInterface(action.payload as ERecordings) || [],
            }
          : widget;
      })
    : [];
};
