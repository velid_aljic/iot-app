import { isNumber } from "lodash";

import { IFilter, mapFilterToEntity, mapFilterToInterface } from "../shared";

import { EDashboard, EDashboards, EPatchWidget, EPostWidget, EPutWidget, ERemoveWidget, EWidget } from "./entities";
import { IDashboard, IDashboards, IPatchWidget, IPostWidget, IPutWidget, IRemoveWidget, IWidget } from "./types";

// RESPONSE MAPPING

export const mapWidgetToInterface = (widget: EWidget): IWidget => {
  const { id, name, chart_type, device_id, filters, created_at, modified_at, ...position } = widget;
  return {
    ...position,
    chartType: chart_type,
    createdAt: created_at,
    data: [],
    deviceId: device_id,
    filters: mapFilterToInterface(filters),
    id,
    isDataStale: false,
    isFailure: false,
    isWorking: false,
    modifiedAt: modified_at,
    name,
  };
};

export const mapWidgetsToInterface = (widgets: EWidget[]): IWidget[] => {
  return widgets.map(mapWidgetToInterface);
};

export const mapDashboardToInterface = (dashboard: EDashboard): IDashboard => {
  const { id, name, widgets, created_at, modified_at } = dashboard;
  return {
    createdAt: created_at,
    id,
    modifiedAt: modified_at,
    name,
    widgets: mapWidgetsToInterface(widgets),
  };
};

export const mapDashboardsToInterface = (dashboards: EDashboards): IDashboards => {
  return dashboards.map(mapDashboardToInterface);
};

// REQUEST MAPPING

export const mapPostWidgetToEntity = (params: IPostWidget): EPostWidget => {
  const { dashboardId, deviceId, name, chartType, filters, ...position } = params;
  return {
    dashboard_id: dashboardId,
    device_id: deviceId,
    name,
    chart_type: chartType,
    filters: mapFilterToEntity(filters),
    ...position,
  };
};

export const mapPatchWidgetToEntity = (params: IPatchWidget): EPatchWidget => {
  const { dashboardId, widgetId, chartType, deviceId, filters, name, x, y, width, height } = params;
  const _chartType = !!chartType ? { chart_type: chartType } : undefined;
  const _deviceId = !!deviceId ? { device_id: deviceId } : undefined;
  const _filters = !!filters ? { filters: mapFilterToEntity(filters) } : undefined;
  const _height = isNumber(height) ? { height } : undefined;
  const _name = !!name ? { name } : undefined;
  const _width = isNumber(width) ? { width } : undefined;
  const _x = isNumber(x) ? { x } : undefined;
  const _y = isNumber(y) ? { y } : undefined;

  return {
    ..._chartType,
    ..._deviceId,
    ..._filters,
    ..._height,
    ..._name,
    ..._width,
    ..._x,
    ..._y,
    dashboard_id: dashboardId,
    widget_id: widgetId,
  };
};

export const mapPutWidgetToEntity = (params: IPutWidget): EPutWidget => {
  const { dashboardId, widgetId, chartType, deviceId, filters, name, ...position } = params;
  return {
    dashboard_id: dashboardId,
    widget_id: widgetId,
    chart_type: chartType,
    device_id: deviceId,
    filters: mapFilterToEntity(filters as IFilter),
    ...position,
  };
};

export const mapRemoveWidgetToEntity = (params: IRemoveWidget): ERemoveWidget => {
  const { dashboardId, widgetId } = params;
  return {
    dashboard_id: dashboardId,
    widget_id: widgetId,
  };
};
