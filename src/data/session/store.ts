import { IResponse } from "utils/http";
import { ASYNC_DISPATCHER_ADAPTER_DEPRECATED, asyncActionCreator } from "utils/redux";

import { login, logout, signup, validate } from "./service";
import StorageDriver from "./StorageDriver";
import { ICredentials, ISignup, IUser } from "./types";

/**
 *
 *
 * Actions
 */
const secondaryAction = (res: any) => StorageDriver.setAuthToken(res.token);
const loginAction = asyncActionCreator<IUser, ICredentials>("LOGIN");
const verifyTokenAction = asyncActionCreator<IUser, undefined>("VERIFY_TOKEN");
const signupAction = asyncActionCreator<IResponse<void>, ISignup>("SIGNUP");

/**
 *
 *
 * Dispatchers
 */
export const asyncLogin = ASYNC_DISPATCHER_ADAPTER_DEPRECATED<IUser, ICredentials>(loginAction, login, secondaryAction);
export const asyncVerifyToken = ASYNC_DISPATCHER_ADAPTER_DEPRECATED<IUser, void>(verifyTokenAction, validate);
export const asyncLogout = ASYNC_DISPATCHER_ADAPTER_DEPRECATED<IUser, void>(verifyTokenAction, logout);
export const asyncSignup = ASYNC_DISPATCHER_ADAPTER_DEPRECATED<IResponse<void>, ISignup>(signupAction, signup);

/**
 *
 *
 * Store
 * State interface & initialState
 */
export interface ISessionStore {
  pristine: boolean;
  isWorking: boolean;
  authenticated: boolean;
}

const initialState: ISessionStore = {
  pristine: true,
  isWorking: false,
  authenticated: false,
};

/**
 *
 *
 * Action types
 */
const LOGIN_STARTED = "LOGIN_STARTED";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGIN_FAILURE = "LOGIN_FAILURE";

const VERIFY_TOKEN_SUCCESS = "VERIFY_TOKEN_SUCCESS";
const VERIFY_TOKEN_FAILURE = "VERIFY_TOKEN_FAILURE";

const SIGNUP_STARTED = "SIGNUP_STARTED";
const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
const SIGNUP_FAILURE = "SIGNUP_FAILURE";

/**
 *
 *
 * Reducer
 * @todo Add action type in reducer function
 */

export const reducer = (state: ISessionStore = initialState, action: any) => {
  switch (action.type) {
    case LOGIN_STARTED:
      return { ...state, isWorking: true };
    case LOGIN_SUCCESS:
      return { ...state, isWorking: false, authenticated: true };
    case LOGIN_FAILURE:
      return { ...state, isWorking: false, authenticated: false };

    case VERIFY_TOKEN_SUCCESS:
      return { ...state, authenticated: true, pristine: false };
    case VERIFY_TOKEN_FAILURE:
      return { ...state, authenticated: false, pristine: false };

    case SIGNUP_STARTED:
      return { ...state, isWorking: true };
    case SIGNUP_SUCCESS:
      return { ...state, isWorking: false };
    case SIGNUP_FAILURE:
      return { ...state, isWorking: false };

    default:
      return state;
  }
};
