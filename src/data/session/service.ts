import { get, IHttpRequestConfig, IHttpResponse, IResponse, post } from "utils/http";
import { compose } from "utils/url";

import StorageDriver from "./StorageDriver";
import { ICredentials, ISignup, IUser } from "./types";

const API = {
  login: "/api/v1/token",
  signup: "/api/v1/accounts",
  validate: "api/v1/token/validate",
};

export const login = (data: ICredentials) => {
  StorageDriver.setAuthToken(null);
  return post<IUser>(API.login, data);
};

export const validate = () => {
  return get<IUser>(API.validate);
};

export const logout = () => {
  StorageDriver.setAuthToken(null);
  return validate();
};

export const signup = (data: ISignup) => {
  const url = compose(API.signup);
  return post<IResponse<void>>(url, data);
};

/**
 *
 *
 * HTTP interceptors for updating authorization token
 */
export const RESPONSE_AUTH_HEADER_KEY = "authorization";

export const onSuccessRequest = (request: IHttpRequestConfig) => {
  const token = StorageDriver.getAuthToken();

  if (token) {
    if (!request.headers) {
      request.headers = {};
    }
    request.headers[RESPONSE_AUTH_HEADER_KEY] = token;
  }
};

export const onSuccessResponse = (response: IHttpResponse) => {
  if (response.headers) {
    StorageDriver.updateAuthToken(response.headers[RESPONSE_AUTH_HEADER_KEY]);
  }
};
