// REQUEST INTERFACES

export interface ICredentials {
  username: string;
  password: string;
}

export interface ISignup extends ICredentials {
  email: string;
}

// RESPONSE INTERFACES

export interface IUser {
  status: string;
  token: string;
}
