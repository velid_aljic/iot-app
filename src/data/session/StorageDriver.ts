import { getItem, getStorage, setItem, TProviderName } from "utils/storage";

const STORAGE_AUTH_TOKEN_KEY = "authorization";
const STORAGE_PROVIDER_KEY = "storageProvider";

class StorageDriver {
  private provider: TProviderName;

  constructor() {
    const providerName = getItem<TProviderName>(STORAGE_PROVIDER_KEY, sessionStorage);
    this.setProvider(providerName);
  }

  public isLocalStorageSession() {
    return this.provider === "localStorage";
  }

  public getAuthKey() {
    return STORAGE_AUTH_TOKEN_KEY;
  }

  public setProvider(name?: TProviderName) {
    this.provider = name && getStorage(name) ? name : "localStorage";
    setItem(STORAGE_PROVIDER_KEY, this.provider, sessionStorage);
  }

  public getAuthToken(): string | undefined {
    return getItem<string>(STORAGE_AUTH_TOKEN_KEY, getStorage(this.provider));
  }

  public setAuthToken(token: string | null) {
    return setItem(STORAGE_AUTH_TOKEN_KEY, token, getStorage(this.provider));
  }

  public updateAuthToken(token: string | undefined) {
    if (token) {
      const prevToken = this.getAuthToken();
      if (token !== prevToken) {
        this.setAuthToken(token);
      }
    }
  }
}

export default new StorageDriver();
