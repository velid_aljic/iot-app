import { asyncActionCreator, asyncDispatcherAdapter } from "utils/redux";

import { ERecordings } from "./entities";
import { getRecordings } from "./service";
import { IGetRecordings } from "./types";

/**
 *
 *
 * Actions
 * <response, params>
 */

export const fetchRecordingsAction = asyncActionCreator<ERecordings, IGetRecordings>("FETCH_RECORDINGS");

/**
 *
 *
 * Dispatchers
 * <response, params>
 */

export const fetchRecordings = asyncDispatcherAdapter<ERecordings, IGetRecordings>(
  fetchRecordingsAction,
  getRecordings,
);
