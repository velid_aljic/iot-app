import { Moment } from "moment";

import { EFilter } from "../shared";

// RESPONSE ENTITIES

export interface ERecording {
  group_recorded_at?: Moment;
  recorded_at?: Moment;
  record_type?: number;
  record_value: number;
}

export type ERecordings = ERecording[];

// REQUEST ENTITIES

export interface EGetRecordings {
  device_id: number;
  filters: EFilter | {};
  widget_id: number;
}

export interface EPostRecording {
  device_id: number;
  record_type: number;
  record_value: number;
  recorded_at: number;
}
