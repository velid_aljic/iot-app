import { mapFilterToEntity } from "../shared/mappers";

import { EGetRecordings, EPostRecording, ERecording, ERecordings } from "./entities";
import { IGetRecordings, IPostRecording, IRecording, IRecordings } from "./types";

/**
 *
 *
 *
 *
 *
 * RESPONSE MAPPING
 */

// RECORDING TO INTERFACE

export const mapRecordingToInterface = (recording: ERecording): IRecording => {
  const { group_recorded_at, recorded_at, record_type, record_value } = recording;

  return {
    groupRecordedAt: group_recorded_at,
    recordedAt: recorded_at,
    recordType: record_type,
    recordValue: record_value,
  };
};

// RECORDING TO ENTITY

export const mapRecordingToEntity = (recording: IRecording): ERecording => {
  const { groupRecordedAt, recordedAt, recordType, recordValue } = recording;

  return {
    group_recorded_at: groupRecordedAt,
    recorded_at: recordedAt,
    record_type: recordType,
    record_value: recordValue,
  };
};

// RECORDINGS TO INTERFACE

export const mapRecordingsToInterface = (recordings: ERecordings): IRecordings => {
  return recordings.map(mapRecordingToInterface);
};

// RECORDINGS TO ENTITY

export const mapRecordingsToEntity = (recordings: IRecordings): ERecordings => {
  return recordings.map(mapRecordingToEntity);
};

/**
 *
 *
 *
 *
 *
 * REQUEST MAPPING
 */

// GET RECORDINGS PARAMS TO ENTITY

export const mapGetRecordingsToEntity = (params: IGetRecordings): EGetRecordings => {
  const { deviceId, filters, widgetId } = params;

  return {
    device_id: deviceId,
    filters: mapFilterToEntity(filters),
    widget_id: widgetId,
  };
};

// POST RECORDING PARAMS TO ENTITY

export const mapPostRecordingToEntity = (params: IPostRecording): EPostRecording => {
  const { deviceId, recordType, recordValue, recordedAt } = params;

  return {
    device_id: deviceId,
    record_type: recordType,
    record_value: recordValue,
    recorded_at: recordedAt,
  };
};
