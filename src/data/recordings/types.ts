import { Moment } from "moment";

import { IFilter } from "data/shared";

// RESPONSE INTERFACES

export interface IRecording {
  groupRecordedAt?: Moment;
  recordedAt?: Moment;
  recordType?: number;
  recordValue: number;
}

export type IRecordings = IRecording[];

// REQUEST INTERFACES

export interface IGetRecordings {
  deviceId: number;
  filters: IFilter;
  widgetId: number;
}

export interface IPostRecording {
  deviceId: number;
  recordType: number;
  recordValue: number;
  recordedAt: number;
}
