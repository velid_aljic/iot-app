import _ from "lodash";

import { IResponse, post } from "utils/http";
import { compose } from "utils/url";

import { ERecordings } from "./entities";
import { mapGetRecordingsToEntity, mapPostRecordingToEntity } from "./mappers";
import { IGetRecordings, IPostRecording, IRecordings } from "./types";

const API = {
  recordings: `/api/v1/devices/{device_id}/recordings`,
  recordingsJSONQL: `/api/v1/devices/{device_id}/recordings/jsonql`,
};

export const getRecordings = (params: IGetRecordings) => {
  const paramsE = mapGetRecordingsToEntity(params);
  const url = compose(
    API.recordingsJSONQL,
    paramsE,
  );

  return post<IResponse<ERecordings>>(url, paramsE.filters);
};

export const postRecording = (params: IPostRecording) => {
  const paramsE = mapPostRecordingToEntity(params);

  const url = compose(
    API.recordings,
    paramsE,
  );

  return post<IResponse<IRecordings>>(url, paramsE);
};
