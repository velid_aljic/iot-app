export const chartTypes = [
  { key: "1", text: "Line", value: "line" },
  { key: "2", text: "Area", value: "area" },
  { key: "3", text: "Bar", value: "bar" },
];

export const orderTypes = [
  { key: "1", text: "Ascending", value: "asc" },
  { key: "2", text: "Descending", value: "desc" },
];

export const selectionTypes = [
  { key: "1", text: "Value", value: "value" },
  { key: "2", text: "Sum", value: "sum" },
  { key: "3", text: "Average", value: "avg" },
  { key: "4", text: "Count", value: "count" },
];

export const groupByTypes = [
  { key: "1", text: "YYYY-MM-DD hh:mm:ss", value: "YYYY-MM-DD HH:MI:SS" },
  { key: "2", text: "YYYY-MM-DD hh:mm", value: "YYYY-MM-DD HH:MI" },
  { key: "3", text: "YYYY-MM-DD hh", value: "YYYY-MM-DD HH" },
  { key: "4", text: "YYYY-MM-DD ", value: "YYYY-MM-DD " },
  { key: "5", text: "YYYY-MM ", value: "YYYY-MM " },
  { key: "6", text: "YYYY ", value: "YYYY " },

  { key: "7", text: "MM-DD hh:mm:ss", value: "MM-DD HH:MI:SS" },
  { key: "8", text: "MM-DD hh:mm", value: "MM-DD HH:MI" },
  { key: "9", text: "MM-DD hh", value: "MM-DD HH" },
  { key: "10", text: "MM-DD", value: "MM-DD" },
  { key: "11", text: "MM", value: "MM" },

  { key: "12", text: "DD hh:mm:ss", value: "DD HH:MI:SS" },
  { key: "13", text: "DD hh:mm", value: "DD HH:MI" },
  { key: "14", text: "DD hh", value: "DD HH" },
  { key: "15", text: "DD", value: "DD" },

  { key: "16", text: "hh:mm:ss", value: "HH:MI:SS" },
  { key: "17", text: "hh:mm", value: "HH:MI" },
  { key: "18", text: "hh", value: "HH" },

  { key: "19", text: "mm:ss", value: "MI:SS" },
  { key: "20", text: "mm", value: "MI" },

  { key: "21", text: "ss", value: "SS" },
];
