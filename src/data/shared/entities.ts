import { Moment } from "moment";

import { TOrders, TSelectionAt, TSelectionValue } from "./types";

// TIMESTAMP ENTITY

export interface ETimestamp {
  created_at: Moment;
  modified_at: Moment;
}

// FILTERING ENTITY

export interface EFilter {
  // filters
  filters: {
    recorded_at: {
      $gt?: Moment;
      $lt?: Moment;
    };
    record_value?: {
      $gt?: number;
      $lt?: number;
    };
  };

  // group by
  groups: {
    recorded_at?: string;
  };

  // order by
  orders: {
    group_recorded_at?: TOrders;
    recorded_at?: TOrders;
  };

  // selections
  selections: {
    record_value: TSelectionValue;
    recorded_at?: TSelectionAt;
  };
}
