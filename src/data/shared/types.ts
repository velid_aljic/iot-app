import { Moment } from "moment";

// Unique
export interface IUnique {
  id: number;
}

// Name
export interface IName {
  name: string;
}

// Timestamp
export interface ITimestamp {
  createdAt: Moment;
  modifiedAt: Moment;
}

// Status
export interface IStatus {
  isFailure: boolean;
  isWorking: boolean;
  isDataStale: boolean;
}

// Position
export interface IPosition {
  x: number;
  y: number;
  width: number;
  height: number;
}

/**
 *
 *
 *
 * FILTERING
 */

export type TOrders = "asc" | "desc";
export type TSelectionAt = "value";
export type TSelectionValue = "value" | "sum" | "avg" | "count";

export interface IFilter {
  // filters
  filters: {
    recordedAt: {
      $gt?: Moment;
      $lt?: Moment;
    };
    recordValue?: {
      $gt?: number;
      $lt?: number;
    };
  };

  // group by
  groups: {
    recordedAt?: string;
  };

  // order by
  orders: {
    groupRecordedAt?: TOrders;
    recordedAt?: TOrders;
  };

  // selections
  selections: {
    recordedAt?: TSelectionAt;
    recordValue: TSelectionValue;
  };
}
