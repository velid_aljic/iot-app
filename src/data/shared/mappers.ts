import { EFilter } from "./entities";
import { IFilter, TSelectionAt, TSelectionValue } from "./types";

/**
 *
 *
 *
 *
 *
 * MAP FILTER TO INTERFACE
 */
export const mapFilterToInterface = (filter: EFilter): IFilter => {
  const { filters, groups, orders, selections } = filter;

  const filtersRecordedAt = Object.assign(
    {},
    !!filters.recorded_at.$gt ? { $gt: filters.recorded_at.$gt } : undefined,
    !!filters.recorded_at.$lt ? { $lt: filters.recorded_at.$lt } : undefined,
  );
  const filtersNew = Object.assign({}, { recordedAt: filtersRecordedAt });

  let groupsNew = {};
  let ordersNew = {};
  if (!!groups.recorded_at) {
    groupsNew = Object.assign({}, { recordedAt: groups.recorded_at });
    ordersNew = Object.assign({}, { groupRecordedAt: orders.group_recorded_at });
  } else {
    ordersNew = Object.assign({}, { recordedAt: orders.recorded_at });
  }

  let selectionsNew = { recordValue: "value" as TSelectionValue };
  if (selections.record_value === "value") {
    selectionsNew = Object.assign({}, { recordValue: "value" as TSelectionValue, recordedAt: "value" as TSelectionAt });
  } else {
    selectionsNew = Object.assign({}, { recordValue: selections.record_value as TSelectionValue });
  }

  return {
    filters: { ...filtersNew },
    groups: { ...groupsNew },
    orders: { ...ordersNew },
    selections: { ...selectionsNew },
  };
};

/**
 *
 *
 *
 *
 *
 * MAP FILTER TO ENTITY
 */
export const mapFilterToEntity = (filter: IFilter): EFilter => {
  const { filters, groups, orders, selections } = filter;

  const filtersRecordedAt = Object.assign(
    {},
    !!filters.recordedAt.$gt ? { $gt: filters.recordedAt.$gt } : undefined,
    !!filters.recordedAt.$lt ? { $lt: filters.recordedAt.$lt } : undefined,
  );
  const filtersNew = Object.assign({}, { recorded_at: filtersRecordedAt });

  let groupsNew = {};
  let ordersNew = {};
  if (!!groups.recordedAt) {
    groupsNew = Object.assign({}, { recorded_at: groups.recordedAt });
    ordersNew = Object.assign({}, { group_recorded_at: orders.groupRecordedAt });
  } else {
    ordersNew = Object.assign({}, { recorded_at: orders.recordedAt });
  }

  let selectionsNew = { record_value: "value" as TSelectionValue };
  if (selections.recordValue === "value") {
    selectionsNew = Object.assign(
      {},
      { record_value: "value" as TSelectionValue, recorded_at: "value" as TSelectionAt },
    );
  } else {
    selectionsNew = Object.assign({}, { record_value: selections.recordValue as TSelectionValue });
  }

  return {
    filters: { ...filtersNew },
    groups: { ...groupsNew },
    orders: { ...ordersNew },
    selections: { ...selectionsNew },
  };
};
