import _, { PartialDeep } from "lodash";
import React from "react";
import Loader from "react-loader";
import { Button, Card, Header, Menu } from "semantic-ui-react";

import { IWidget } from "./types";

import "./index.scss";

// INTERFACES

interface Props<A, C, D, E> {
  widget: IWidget;

  menuClassName: string;

  identites: A & C;

  fetchId: string[];
  updateId: string[];

  fetchRecordings: (params: PartialDeep<C>) => Promise<E>;
  updateWidget: (params: PartialDeep<A>) => Promise<D>;

  toggleDelete: () => void;
  toggleEdit: (widgetId?: number) => void;
}

interface State {}

// CLASS

export class Widget<A, C, D, E> extends React.Component<Props<A, C, D, E>, State> {
  // LIFECYLE METHODS

  componentDidMount() {
    this.fetchData(this.props);
  }

  componentDidUpdate(prevProps: Props<A, C, D, E>) {
    const { isDataStale } = this.props.widget;

    /* Fetch data if filters/options have changed */
    if (isDataStale && isDataStale !== prevProps.widget.isDataStale) {
      this.fetchData(this.props);
    }
  }

  // COMPONENT METHODS

  fetchData = (props: Props<A, C, D, E>) => {
    const { identites, fetchId } = props;
    const fetchParams = _.pick(identites, fetchId);
    this.props.fetchRecordings(fetchParams);
  };

  // RENDER METHODS

  renderError = () => {
    return <Header>Fatal error</Header>;
  };

  renderMenu = (props: Props<A, C, D, E>) => {
    const { toggleDelete, toggleEdit, menuClassName, widget } = props;

    return (
      <Menu borderless fluid secondary size="mini">
        <Menu.Menu className="widgetNameMenu">
          <Menu.Item>
            <Header>{widget.name}</Header>
          </Menu.Item>
        </Menu.Menu>
        <Menu.Item />
        <Menu.Menu position="right" className={menuClassName}>
          <Button circular size="mini" icon="refresh" onClick={() => this.fetchData(props)} />
          <Button circular size="mini" icon="settings" onClick={() => toggleEdit(widget.id)} />
          <Button circular size="mini" icon="trash" onClick={toggleDelete} />
        </Menu.Menu>
      </Menu>
    );
  };

  // RENDER

  render() {
    const { widget, children } = this.props;

    return (
      <>
        <div className="loaderCard">
          <Loader loaded={!widget.isWorking}>
            <Card fluid className="widgetCard">
              <Card.Content className="widgetMenu">
                <Card.Header>{this.renderMenu(this.props)}</Card.Header>
              </Card.Content>
              <Card.Content>{widget.isFailure ? this.renderError() : children}</Card.Content>
            </Card>
          </Loader>
        </div>
      </>
    );
  }
}
