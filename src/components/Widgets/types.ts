export interface IWidget {
  id: number;
  name: string;
  data: object[];
  chartType: string;

  isFailure: boolean;
  isWorking: boolean;
  isDataStale: boolean;
}
