import React from "react";
import { CartesianGrid, Line, LineChart as ReLineChart, Tooltip, XAxis, YAxis } from "recharts";

import { margin } from "./defaults";
import { IData } from "./types";

interface Props extends IData {
  width: number;
  height: number;
  xAxis: string;
  dataKey: string;

  lineStroke?: string;
  cartesianStroke?: string;
}

export class LineChart extends React.PureComponent<Props> {
  static defaultProps = {
    lineStroke: "#ff7300",
    cartesianStroke: "#f5f5f5",
  };

  render() {
    const { data, dataKey, xAxis, height, width, lineStroke, cartesianStroke } = this.props;

    return (
      <ReLineChart width={width} height={height} data={data} margin={margin}>
        <YAxis />
        <XAxis dataKey={xAxis} />
        <Tooltip />
        <CartesianGrid stroke={cartesianStroke} />
        <Line type="monotone" connectNulls={true} dataKey={dataKey} stroke={lineStroke} yAxisId={0} />
      </ReLineChart>
    );
  }
}
