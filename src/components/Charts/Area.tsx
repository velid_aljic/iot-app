import React from "react";
import { Area, AreaChart as ReAreaChart, CartesianGrid, Tooltip, XAxis, YAxis } from "recharts";

import { margin } from "./defaults";
import { IData } from "./types";

interface Props extends IData {
  width: number;
  height: number;
  xAxis: string;
  dataKey: string;

  lineStroke?: string;
  cartesianStroke?: string;
}

export class AreaChart extends React.PureComponent<Props> {
  static defaultProps = {
    lineStroke: "#8884d8",
    cartesianStroke: "#f5f5f5",
  };

  render() {
    const { data, dataKey, xAxis, height, width, lineStroke, cartesianStroke } = this.props;

    return (
      <ReAreaChart width={width} height={height} data={data} margin={margin}>
        <defs>
          <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
            <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
          </linearGradient>
        </defs>
        <YAxis />
        <XAxis dataKey={xAxis} />
        <Tooltip />
        <CartesianGrid stroke={cartesianStroke} />
        />
        <Area
          type="monotone"
          connectNulls={true}
          dataKey={dataKey}
          stroke={lineStroke}
          yAxisId={0}
          fill="url(#colorUv)"
        />
      </ReAreaChart>
    );
  }
}
