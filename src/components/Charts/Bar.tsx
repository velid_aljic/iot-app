import React from "react";
import { Bar, BarChart as ReBarChart, CartesianGrid, Tooltip, XAxis, YAxis } from "recharts";

import { margin } from "./defaults";
import { IData } from "./types";

interface Props extends IData {
  width: number;
  height: number;
  xAxis: string;
  dataKey: string;

  lineStroke?: string;
  cartesianStroke?: string;
}

export class BarChart extends React.PureComponent<Props> {
  static defaultProps = {
    lineStroke: "#82ca9d",
    cartesianStroke: "#f5f5f5",
  };

  render() {
    const { data, dataKey, xAxis, height, width, lineStroke, cartesianStroke } = this.props;

    return (
      <ReBarChart width={width} height={height} data={data} margin={margin}>
        <YAxis />
        <XAxis dataKey={xAxis} />
        <Tooltip />
        <CartesianGrid stroke={cartesianStroke} />
        <Bar dataKey={dataKey} fill={lineStroke} yAxisId={0} />
      </ReBarChart>
    );
  }
}
