import React from "react";
import { ResponsiveContainer } from "recharts";

const heightOffset = 80;

interface Props {
  height: number;
}

export class ChartContainer extends React.PureComponent<Props> {
  render() {
    const { height, children } = this.props;
    return (
      <ResponsiveContainer width="100%" height={height - heightOffset}>
        {children}
      </ResponsiveContainer>
    );
  }
}
