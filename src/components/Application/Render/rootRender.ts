import { render } from "react-dom";

export const rootRender = (Component: JSX.Element) => render(Component, document.getElementById("root"));
