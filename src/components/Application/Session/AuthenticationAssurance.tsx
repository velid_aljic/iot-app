import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";

import ErrorBus from "components/Application/Errors";

import { IRootStore } from "store";

const LOGIN_PATH = "/login";

interface Props extends RouteComponentProps {
  authenticated: boolean;
}

const mapStateToProps = (state: IRootStore) => {
  const { authenticated } = state.session;

  return {
    authenticated,
  };
};

class AuthenticationAssuranceComponent extends React.PureComponent<Props> {
  subscriptionToken: string;

  componentDidMount() {
    if (!this.props.authenticated) {
      this.goToLogin();
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (!this.props.authenticated && prevProps.authenticated) {
      this.goToLogin();
    }
  }

  componentWillMount() {
    this.subscriptionToken = ErrorBus.subscribeOnce("HTTP/401", this.handleUnauthorizedApiCall.bind(this));
  }

  componentWillUnmount() {
    ErrorBus.unsubscribe(this.subscriptionToken);
  }

  goToLogin() {
    this.props.history.replace(LOGIN_PATH);
  }

  handleUnauthorizedApiCall() {
    // @todo: Add appropriate handler
    // tslint:disable-next-line:no-console
    console.error("AuthenticationAssurance: handleUnauthorizedApiCall");
  }

  render() {
    if (this.props.authenticated) {
      return this.props.children;
    }
    return null;
  }
}

export const AuthenticationAssurance = withRouter(connect(mapStateToProps)(AuthenticationAssuranceComponent));
