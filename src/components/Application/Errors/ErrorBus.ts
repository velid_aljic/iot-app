import isNumber from "lodash/isNumber";

import { HttpError } from "utils/http";
import { PubSub, TListener } from "utils/pubSub";

type TErrorType =
  | "HTTP/400"
  | "HTTP/401"
  | "HTTP/403"
  | "HTTP/404"
  | "HTTP/5XX"
  | "HTTP/TIMEOUT"
  | "HTTP/OFFLINE"
  | "HTTP/NETWORK"
  | "APP/UNCAUGHT";

class ErrorBus {
  pubSub = new PubSub();

  publishHttp(error: HttpError) {
    if (!navigator.onLine) {
      return this.pubSub.publish("HTTP/OFFLINE", error);
    }

    if (isNumber(error.status) && error.status >= 500) {
      return this.pubSub.publish("HTTP/5XX", error);
    }

    if (error.status === 400) {
      return this.pubSub.publish("HTTP/400", error);
    }

    if (error.status === 401) {
      return this.pubSub.publish("HTTP/401", error);
    }

    if (error.status === 403) {
      return this.pubSub.publish("HTTP/403", error);
    }

    if (error.status === 404) {
      return this.pubSub.publish("HTTP/404", error);
    }

    if (!error.network) {
      return this.pubSub.publish("HTTP/NETWORK", error);
    }

    if (error.timeout) {
      return this.pubSub.publish("HTTP/TIMEOUT", error);
    }
  }

  publish(topic: TErrorType, data: Error) {
    return this.pubSub.publish(topic, data);
  }

  subscribe(topic: TErrorType, listener: TListener) {
    return this.pubSub.subscribe(topic, listener);
  }

  subscribeOnce(topic: TErrorType, listener: TListener) {
    return this.pubSub.subscribeOnce(topic, listener);
  }

  unsubscribe(token: string) {
    return this.pubSub.unsubscribe(token);
  }
}

export default new ErrorBus();
