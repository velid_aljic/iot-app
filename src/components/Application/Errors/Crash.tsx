import React from "react";

interface Props {}

const style = {
  paddingTop: "20%",
  display: "table",
  margin: "0 auto",
};

/**
 *
 *
 * @export
 * @class Crash
 * @extends {React.PureComponent<Props>}
 * @todo: Fix styling and relocate from inline
 */
export class Crash extends React.PureComponent<Props> {
  render() {
    return (
      <div style={style}>
        <h2>&#3232;_&#3232; Ooops</h2>
      </div>
    );
  }
}
