import React from "react";

import { rootRender } from "components/Application/Render";
import { CreateToast } from "components/Messages";

import { HttpError } from "utils/http";

import { Crash } from "./Crash";
import ErrorBus from "./ErrorBus";
import { IServerErrorPayload } from "./types";

interface Props {}

/**
 *
 *
 * @description Component provides handlers for various types of errors
 * @todo: Implement component for presenting handled errors instead of console.error
 * @todo: Remove handler for HTTP/401 event
 */

// tslint:disable
export class ErrorsBoundry extends React.PureComponent<Props> {
  appUncaughtSub: string;
  badRequestErrorSub: string;
  forbiddenErrorSub: string;
  networkErrorSub: string;
  notFoundErroSub: string;
  offlineErrorSub: string;
  serverErrorSub: string;
  timeoutErrorSub: string;
  unauthorizedErrorSub: string;

  componentWillMount() {
    this.appUncaughtSub = ErrorBus.subscribeOnce("APP/UNCAUGHT", this.handleAppUncaughtError);

    this.badRequestErrorSub = ErrorBus.subscribe("HTTP/400", this.handleServerErrors);
    this.forbiddenErrorSub = ErrorBus.subscribe("HTTP/403", this.handleForbiddenError);
    this.networkErrorSub = ErrorBus.subscribe("HTTP/NETWORK", this.handleNetworkError);
    this.notFoundErroSub = ErrorBus.subscribe("HTTP/404", this.handleNotFoundError);
    this.offlineErrorSub = ErrorBus.subscribe("HTTP/OFFLINE", this.handleOfflineError);
    this.serverErrorSub = ErrorBus.subscribe("HTTP/5XX", this.handleServerErrors);
    this.timeoutErrorSub = ErrorBus.subscribe("HTTP/TIMEOUT", this.handleTimeoutError);
    this.unauthorizedErrorSub = ErrorBus.subscribe("HTTP/401", this.handleUnauthorizedError);
  }

  componentWillUnmount() {
    ErrorBus.unsubscribe(this.serverErrorSub);
    ErrorBus.unsubscribe(this.timeoutErrorSub);
    ErrorBus.unsubscribe(this.offlineErrorSub);
    ErrorBus.unsubscribe(this.networkErrorSub);

    ErrorBus.unsubscribe(this.appUncaughtSub);
  }

  handleServerErrors = (error: HttpError<IServerErrorPayload>) => {
    const payload = error.payload || ({} as IServerErrorPayload);
    const { error: title, message } = payload;

    console.error("ErrorBoundry: title: ", title, ", message: ", message ? message : error.message);
  };

  handleForbiddenError = (_error: HttpError) => {
    CreateToast({ icon: "close", message: "Request forbidden" });
    console.error("ErrorBoundry: Request forbidden");
  };

  handleTimeoutError = (_error: HttpError) => {
    CreateToast({ icon: "close", message: "Timeout exceeded" });
    console.error("ErrorBoundry: Timeout exceeded");
  };

  handleOfflineError = (_error: HttpError) => {
    CreateToast({ icon: "close", message: "Check your internet connection" });
    console.error("ErrorBoundry: Check your internet connection");
  };

  handleNetworkError = (_error: HttpError) => {
    CreateToast({ icon: "close", message: "Network problem detected" });
    console.error("ErrorBoundry: Network problem detected");
  };

  handleNotFoundError = (_error: HttpError) => {
    CreateToast({ icon: "close", message: "Page not found" });
    console.error("ErrorBoundry: Page not found error");
  };

  handleUnauthorizedError = (_error: HttpError) => {
    CreateToast({ icon: "close", message: "Unauthorized access" });
    console.error("ErrorBoundry: Unauthorized access");
  };

  handleAppUncaughtError = (_error: Error) => {
    rootRender(<Crash />);
  };

  componentDidCatch(error: Error) {
    this.handleAppUncaughtError(error);
  }

  render() {
    return this.props.children;
  }
}
