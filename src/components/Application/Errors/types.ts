export interface IServerErrorPayload {
  timestamp: string;
  status: number;
  error: string;
  message: string;
  path: string;
  exception: string;
}
