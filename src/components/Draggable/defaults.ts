import { transformRelative } from "./utils";

export const DEFAULT_PRECISION = 100000;

export const CANCEL = "rndCancel";
export const BOUNDS = "rndBounds";
export const CHILDREN = "rndChildren";

const MIN_HEIGHT = 180;
const MIN_WIDTH = 180;
const MAX_HEIGHT = 2000;
const MAX_WIDTH = 2000;
const DEFAULT_X = 0;
const DEFAULT_Y = 0;

export const DEFAULT_AXIS = {
  x: DEFAULT_X,
  y: DEFAULT_Y,
};

export const DEFAULT_SIZE = {
  minHeight: MIN_HEIGHT,
  minWidth: MIN_WIDTH,
  maxHeight: MAX_HEIGHT,
  maxWidth: MAX_WIDTH,
  ...DEFAULT_AXIS,
};

export const RND_DEFAULTS = {
  cancel: `.${CANCEL}`,
  bounds: `.${BOUNDS}`,
  ...DEFAULT_SIZE,
};

export const DEFAULT_RELATIVE_POSITION = {
  ...transformRelative({
    height: DEFAULT_SIZE.minHeight,
    width: DEFAULT_SIZE.minWidth,
    x: DEFAULT_AXIS.x,
    y: DEFAULT_AXIS.y,
  })(window),
};
