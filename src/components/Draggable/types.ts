export interface IPosition {
  height: number;
  width: number;
  x: number;
  y: number;
}
