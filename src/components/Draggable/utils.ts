import { DEFAULT_PRECISION } from "./defaults";
import { IPosition } from "./types";

export const toNumber = (dimension: string | number) => {
  return typeof dimension === "string" ? parseFloat(dimension) : dimension;
};

export const toAbsolute = (reference: number) => (dimension: string | number) => {
  return (toNumber(dimension) / DEFAULT_PRECISION) * reference;
};

export const toRelative = (reference: number) => (dimension: string | number) => {
  return (toNumber(dimension) / reference) * DEFAULT_PRECISION;
};

export const transformPosition = (transformer: (reference: any) => (dimension: any) => number) => ({
  height,
  width,
  x,
  y,
}: IPosition) => ({ innerHeight, innerWidth }: Window) => ({
  height: transformer(innerHeight)(height),
  width: transformer(innerWidth)(width),
  x: transformer(innerWidth)(x),
  y: transformer(innerHeight)(y),
});

export const transformAbsolute = transformPosition(toAbsolute);
export const transformRelative = transformPosition(toRelative);
