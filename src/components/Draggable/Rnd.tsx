import React from "react";
import { Rnd } from "react-rnd";

import { CHILDREN, RND_DEFAULTS } from "./defaults";
import { IPosition } from "./types";
import { transformAbsolute, transformRelative } from "./utils";

import "./index.scss";

// INTERFACES

type TPosition<T> = T | IPosition;

interface Props<M, P> extends IPosition {
  params: object;
  update: (params: TPosition<M>) => Promise<P>;
}

interface State extends IPosition {
  zIndex: number;
}

// POSITION CONTEXT

export const PositionContext = React.createContext({
  height: RND_DEFAULTS.minHeight,
  width: RND_DEFAULTS.minWidth,
  x: RND_DEFAULTS.x,
  y: RND_DEFAULTS.y,
});

// CLASS

export class RndComponent<M, P> extends React.PureComponent<Props<M, P>, State> {
  // STATIC

  static zIndex = 1;

  static zIndexIncrement = () => {
    return ++RndComponent.zIndex;
  };

  static zIndexIncrementConditional = (zIndex: number) => {
    return zIndex < RndComponent.zIndex ? ++RndComponent.zIndex : zIndex;
  };

  // CONSTRUCTOR

  constructor(props: Props<M, P>) {
    super(props);
    this.state = { ...this.recalculate(props), zIndex: RndComponent.zIndexIncrement() };
  }

  // LIFECYLE METHODS

  componentDidMount() {
    window.addEventListener("resize", this.reposition);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.reposition);
  }

  // COMPONENT METHODS

  reposition = () => {
    this.setState(this.recalculate(this.props));
  };

  recalculate = (props: Props<M, P>) => {
    const { height, width, x, y } = props;
    return transformAbsolute({ height, width, x, y })(window);
  };
  // RND METHODS

  onDragStart = () => {
    const { zIndex } = this.state;
    this.setState({ zIndex: RndComponent.zIndexIncrementConditional(zIndex) });
  };

  onResize = (e: any, direction: any, ref: any, delta: any, position: any) => {
    const { zIndex } = this.state;
    const { width, height } = ref.style;
    this.setState({ width, height, ...position, zIndex: RndComponent.zIndexIncrementConditional(zIndex) });
  };

  onResizeStop = (e: any, direction: any, ref: any, delta: any, position: any) => {
    const { params } = this.props;
    const { width, height, x, y, zIndex } = this.state;

    this.setState({ zIndex: RndComponent.zIndexIncrementConditional(zIndex) }, () => {
      this.props.update({
        ...params,
        ...transformRelative({ height, width, x, y })(window),
      });
    });
  };

  onDragStop = (e: any, d: any) => {
    const { params } = this.props;
    const { height, width, zIndex } = this.state;
    const { x, y } = d;

    this.setState({ x, y, zIndex: RndComponent.zIndexIncrementConditional(zIndex) }, () => {
      this.props.update({
        ...params,
        ...transformRelative({ height, width, x, y })(window),
      });
    });
  };

  // RENDER

  render() {
    const { height, width, x, y, zIndex } = this.state;
    const { children } = this.props;

    return (
      <Rnd
        {...RND_DEFAULTS}
        style={{ zIndex }}
        position={{ x, y }}
        size={{ width, height }}
        onDragStart={this.onDragStart}
        onResize={this.onResize}
        onDragStop={this.onDragStop}
        onResizeStop={this.onResizeStop}>
        <PositionContext.Provider value={this.state}>
          <div className={CHILDREN}>{children}</div>
        </PositionContext.Provider>
      </Rnd>
    );
  }
}
