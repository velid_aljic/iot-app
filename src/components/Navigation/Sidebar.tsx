import React from "react";
import { Icon, Menu, Sidebar as SemanticSidebar } from "semantic-ui-react";

import { IRoute } from "./types";

// INTERFACE

interface Props {
  routes: IRoute[];
  className: string;
  handleClick: (path: string) => void;
}

// CLASS

export class Sidebar extends React.PureComponent<Props> {
  // RENDER METHODS

  renderItems = (routes: IRoute[]) => {
    return routes.map(this.rendeItem);
  };

  rendeItem = (route: IRoute) => {
    return (
      <Menu.Item
        icon
        as="a"
        key={`sidebar_${route.label}`}
        path={route.path}
        onClick={(e, { path }) => this.props.handleClick(path)}>
        <Icon name={route.icon} />
        {route.label}
      </Menu.Item>
    );
  };

  // RENDER

  render() {
    const { className, routes } = this.props;

    return (
      <>
        <SemanticSidebar
          as={Menu}
          visible={true}
          className={className}
          direction="left"
          icon="labeled"
          width="thin"
          vertical>
          {this.renderItems(routes)}
        </SemanticSidebar>
      </>
    );
  }
}
