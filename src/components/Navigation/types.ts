import { SemanticICONS } from "semantic-ui-react";

export interface IRoute {
  exact?: boolean;
  label?: string;
  name: string;
  path: string;
  inSidebar?: boolean;
  rank?: number;
  icon?: SemanticICONS;
}
