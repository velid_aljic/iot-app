import React, { ReactNode } from "react";
import { Container } from "semantic-ui-react";

import { Sidebar } from "./Sidebar";
import { Topbar } from "./Topbar";
import { IRoute } from "./types";

import "./index.scss";

// INTERFACES

interface Props {
  logo: string;
  children: ReactNode;
  sidebarRoutes: IRoute[];
  contextInfo: string;
  contextMenu: string;
  logout: () => void;
  forward: (path: string) => void;
}

interface State {}

// CLASS

export class Navigation extends React.PureComponent<Props, State> {
  // COMPONENT METHODS

  forward = (path: string) => this.props.forward(path);
  logout = () => this.props.logout();

  // RENDER

  render() {
    const { logo, children, sidebarRoutes, contextMenu, contextInfo } = this.props;

    return (
      <>
        <div className="nav-wrapper">
          <div className="nav-top-section">
            <div className="nav-logo-section">
              <img className="app-logo" src={logo} />
            </div>

            <div className="nav-menu-section">
              <Topbar
                className="nav-topbar"
                contextMenu={contextMenu}
                contextInfo={contextInfo}
                handleLogout={this.logout}
              />
            </div>
          </div>

          <div className="nav-bottom-section">
            <div className="nav-sidebar-section">
              <Sidebar className="nav-sidebar" routes={sidebarRoutes} handleClick={this.forward} />
            </div>

            <div className="nav-content-section">
              <Container fluid className="nav-content-container">
                {children}
              </Container>
            </div>
          </div>
        </div>
      </>
    );
  }
}
