import React from "react";
import { Button, Dropdown, Menu } from "semantic-ui-react";

// INTERFACE

interface Props {
  contextInfo: string;
  contextMenu: string;
  className: string;
  handleLogout: () => void;
}

// CLASS

export class Topbar extends React.PureComponent<Props> {
  // RENDER

  render() {
    const { contextInfo, contextMenu, className, handleLogout } = this.props;

    return (
      <>
        <Menu className={className} size="large" borderless fluid stackable>
          <Menu.Menu id={contextInfo} position="left" />
          <Menu.Menu id={contextMenu} position="right" />
          <Menu.Menu>
            <Dropdown trigger={<Button icon="user" circular />} item>
              <Dropdown.Menu direction="left">
                <Dropdown.Item icon="edit" text="Edit Profile" />
                <Dropdown.Item icon="sign-out" text="Sign Out" onClick={handleLogout} />
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Menu>
      </>
    );
  }
}
