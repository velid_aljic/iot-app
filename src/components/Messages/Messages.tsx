import React from "react";
import { toast as tostify } from "react-toastify";
import { Header, Icon } from "semantic-ui-react";

import "react-toastify/dist/ReactToastify.css";

const style = {
  textAlign: "center",
};

export const CreateToast = ({ icon, message }: any) => {
  tostify(
    <div>
      <Header as="h5" style={style}>
        <Icon size="large" color="black" name={icon} />
        {message}
      </Header>
    </div>,
    {
      closeOnClick: true,
      pauseOnHover: false,
      hideProgressBar: true,
      position: tostify.POSITION.BOTTOM_RIGHT,
    },
  );
};
