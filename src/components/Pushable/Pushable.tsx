import React from "react";
import { Container, Segment, Sidebar } from "semantic-ui-react";

import "./index.scss";

// INTERFACE

interface Props {
  open: boolean;
  forms: React.ReactNode;
}

// COMPONENT

export class Pushable extends React.PureComponent<Props> {
  render() {
    const { forms, open, children } = this.props;

    return (
      <Sidebar.Pushable>
        <Sidebar animation="overlay" width="very wide" direction="right" as={Segment} visible={open}>
          <Container className="formContainer">{forms}</Container>
        </Sidebar>

        <Sidebar.Pusher dimmed={open} className="childrenContainer">
          {children}
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    );
  }
}
