import React from "react";
import { connect } from "react-redux";
import { submit } from "redux-form";
import { Button, Icon } from "semantic-ui-react";

// @TODO: ADD PROP TYPES

export interface ISubmitButton {
  formName: string;
  isWorking: boolean;
}

const ButtonComponent = ({ formName, isWorking }: ISubmitButton) => ({ dispatch }: any) => (
  <Button icon type="Button" loading={isWorking} disabled={isWorking} onClick={() => dispatch(submit(formName))}>
    <Icon name="save" size="big" />
  </Button>
);

export const RemoteSubmitButton = ({ formName, isWorking }: ISubmitButton) =>
  connect()(ButtonComponent({ formName, isWorking }));
