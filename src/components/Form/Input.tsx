import React from "react";
import { Form, Icon, Input as SemanticInput } from "semantic-ui-react";

// @TODO: ADD PROP TYPES

export const Input = ({ input, label, placeholder, meta: { error, touched }, width, required, ...custom }: any) => {
  const errorMessage = (
    <div style={{ color: "#E20000", paddingTop: ".3rem", fontSize: "12px", float: "left" }}>
      <Icon name="warning" />
      {error}
    </div>
  );

  return (
    <Form.Field required={required} width={width}>
      <label style={{ float: "left" }}>{label}</label>

      <SemanticInput
        {...input}
        {...custom}
        autoComplete="off"
        value={input.value}
        error={touched && !!error}
        placeholder={placeholder}
        onChange={(param, data) => input.onChange(data.value)}
      />
      {touched && !!error && errorMessage}
    </Form.Field>
  );
};
