import React from "react";
import { Dropdown as SemanticDropdown, Form, Icon } from "semantic-ui-react";

// @TODO: ADD PROP TYPES

export const Dropdown = ({ input, label, placeholder, meta: { error, touched }, width, required, ...custom }: any) => {
  const errorMessage = (
    <div style={{ color: "#E20000", paddingTop: ".3rem", fontSize: "12px", float: "left" }}>
      <Icon name="warning" />
      {error}
    </div>
  );

  return (
    <Form.Field required={required} width={width}>
      <label style={{ float: "left" }}>{label}</label>

      <SemanticDropdown
        selection
        clearable
        {...input}
        {...custom}
        value={input.value}
        error={touched && !!error}
        placeholder={placeholder}
        onChange={(param, data) => input.onChange(data.value)}
      />
      {touched && !!error && errorMessage}
    </Form.Field>
  );
};
