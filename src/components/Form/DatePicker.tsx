import React from "react";
import ReactDatePicker from "react-datepicker";
import { Form, Icon } from "semantic-ui-react";

import "react-datepicker/dist/react-datepicker.css";

const DATE_FORMAT = "YYYY-MM-DD hh:mm";

// @TODO: ADD PROP TYPES

export const DatePicker = ({ input, label, placeholder, meta: { error, touched }, width }: any) => {
  const errorMessage = (
    <div style={{ color: "#E20000", paddingTop: ".3rem", fontSize: "12px" }}>
      <Icon name="warning" />
      {error}
    </div>
  );

  return (
    <Form.Field width={width}>
      <label>{label}</label>

      <ReactDatePicker
        // showTimeSelect
        dateFormat={DATE_FORMAT}
        placeholderText={placeholder}
        selected={typeof input.value === "string" ? null : input.value}
        onChange={data => input.onChange(data)}
      />
      {touched && error && errorMessage}
    </Form.Field>
  );
};
