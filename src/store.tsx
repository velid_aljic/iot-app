import { applyMiddleware, combineReducers, createStore } from "redux";
import { reducer as reduxFormReducer } from "redux-form";

import thunk from "redux-thunk";

import { IDashboardsStore, reducer as dashboardsReducer } from "data/dashboard";
import { IDevicesStore, reducer as devicesReducer } from "data/devices";
import { ISessionStore, reducer as sessionReducer } from "data/session";

export interface IRootStore {
  dashboards: IDashboardsStore;
  devices: IDevicesStore;
  session: ISessionStore;
}

export const reducers = combineReducers({
  dashboards: dashboardsReducer,
  devices: devicesReducer,
  form: reduxFormReducer,
  session: sessionReducer,
});

export const store = createStore(
  reducers,
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk),
);
