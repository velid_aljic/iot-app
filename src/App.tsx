import * as React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";

import http, { HttpError } from "utils/http";

import { onSuccessRequest, onSuccessResponse } from "data/session";

import ErrorBus, { ErrorsBoundry } from "components/Application/Errors";

import { store } from "store";
import { Pages } from "views";

http.interceptors.request.use(request => {
  onSuccessRequest(request);
  return request;
});

http.interceptors.response.use(
  response => {
    onSuccessResponse(response);
    return response;
  },
  (error: HttpError) => {
    ErrorBus.publishHttp(error);
    return Promise.reject(error.message || error);
  },
);

window.onerror = (msg: string) => ErrorBus.publish("APP/UNCAUGHT", new Error(msg));

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <ErrorsBoundry>
          <BrowserRouter>
            <Route component={Pages} />
          </BrowserRouter>
        </ErrorsBoundry>
      </Provider>
    );
  }
}

export default App;
