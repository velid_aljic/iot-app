import axios, { AxiosRequestConfig } from "axios";

import { execute } from "./core";

interface IPayload {}

/**
 *
 *
 *  CRUD wrappers
 */
export const get = <T>(url: string, options?: AxiosRequestConfig) => {
  return execute<T>(url, options);
};

export const post = <T>(url: string, data?: IPayload, options?: AxiosRequestConfig) => {
  const request = Object.assign({}, options, { data, method: "POST" });
  return execute<T>(url, request);
};

export const put = <T>(url: string, data?: IPayload, options?: AxiosRequestConfig) => {
  const request = Object.assign({}, options, { data, method: "PUT" });
  return execute<T>(url, request);
};

export const patch = <T>(url: string, data?: IPayload, options?: AxiosRequestConfig) => {
  const request = Object.assign({}, options, { data, method: "PATCH" });
  return execute<T>(url, request);
};

export const remove = <T>(url: string, options?: AxiosRequestConfig) => {
  const request = Object.assign({}, options, { method: "DELETE" });
  return execute<T>(url, request);
};

/**
 *
 *
 *  Http requests base url
 *  @todo Relocate axios configuration to another file
 */
const API_BASE_URL = "https://final-iot-backend.herokuapp.com";
axios.defaults.baseURL = API_BASE_URL;

export default axios;
