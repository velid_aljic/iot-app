import axios from "axios";

export const getCancelTokenSource = () => {
  return axios.CancelToken.source();
};
