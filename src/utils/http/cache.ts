export interface IBufferRecord<P = {}> {
  pending?: boolean;
  results?: P;
  promise?: PromiseLike<P>;
}

const moduleBuffer = new Map<string, IBufferRecord>();

export function withCachedBuffer<P>(fn: () => Promise<P>, key: string, buffer = moduleBuffer) {
  return new Promise<P>((resolve, reject) => {
    if (buffer.has(key)) {
      const entry = buffer.get(key);
      if (entry) {
        if (entry.pending) {
          return resolve(entry.promise as Promise<P>);
        }

        return resolve(entry.results as P);
      }
    }

    const promise = fn();
    buffer.set(key, { pending: true, promise });

    return promise.then(
      results => {
        buffer.set(key, {
          pending: false,
          results,
        });

        return resolve(results);
      },
      err => {
        buffer.delete(key);
        return reject(err);
      },
    );
  });
}
