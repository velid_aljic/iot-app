import { AxiosRequestConfig, AxiosResponse, CancelTokenSource } from "axios";

export interface IHttpRequestConfig extends AxiosRequestConfig {}

export interface IHttpResponse extends AxiosResponse {}

export interface IHttpCancelTokenSource extends CancelTokenSource {}

/**
 *
 *
 * Interfaces modeled by server response specifications.
 * Proposal for renaming content to payload in next update of API specification.
 * API author: Ensar Sarajcic
 */
export interface IError {
  status: string;
  message: string;
}

export interface IResponse<T> {
  content?: T;
  error?: IError;
}
