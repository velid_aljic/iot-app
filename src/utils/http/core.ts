import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

import { HttpError } from "./error";

/**
 *
 *
 *  Axios interceptors
 */
axios.interceptors.response.use(
  response => Promise.resolve(onSuccess(response)),
  error => Promise.reject(onFailure(error)),
);

/**
 *
 *
 *  onSuccess callback
 */
const onSuccess = (response: AxiosResponse) => {
  if (!response.data) {
    response.data = {};
  }
  return response;
};

/**
 *
 *
 *  onFailure callback
 */
const onFailure = (error: AxiosError) => {
  if (error.response) {
    const { data: payload, status, statusText } = error.response;
    return new HttpError({ payload, status, message: statusText });
  }

  const { message } = error;

  /**
   * Canceled by user action
   */
  if (axios.isCancel(error)) {
    return new HttpError({ message, canceled: true, network: true });
  }

  /**
   * Timeout exceeded
   */
  if (error.code === "ECONNABORTED") {
    return new HttpError({ message, timeout: true, network: true });
  }

  /**
   * Network error
   */
  return new HttpError({ message, network: false });
};

/**
 *
 *
 *  Axios wrapper
 *  Resolves http requests
 */
export const execute = async <T>(url: string, options: AxiosRequestConfig = {}) => {
  const response = await axios(url, options);
  return response.data as T;
};
