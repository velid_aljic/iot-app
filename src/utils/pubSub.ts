export type TListener = (data?: any) => any;

export interface ISubscriber {
  [token: string]: TListener;
}

export interface ITopics {
  [topic: string]: ISubscriber;
}

let uuid = 0;

export class PubSub {
  topics = {} as ITopics;

  subscribe = (topic: string, listener: TListener) => {
    if (!this.topics.hasOwnProperty(topic)) {
      this.topics[topic] = {} as ISubscriber;
    }

    const token = `uuid_${uuid++}`;
    this.topics[topic][token] = listener;

    return token;
  };

  subscribeOnce = (topic: string, listener: TListener) => {
    const self = this;
    const token = this.subscribe(topic, function() {
      self.unsubscribe(token);
      listener.apply(this, arguments);
    });

    return token;
  };

  unsubscribe = (token: string) => {
    for (const topic in this.topics) {
      if (this.topics.hasOwnProperty(topic) && this.topics[topic].hasOwnProperty(token)) {
        delete this.topics[topic][token];
      }
    }
  };

  publish = (topic: string, data?: any) => {
    if (!this.topics.hasOwnProperty(topic)) {
      return;
    }

    for (const token in this.topics[topic]) {
      if (this.topics[topic].hasOwnProperty(token)) {
        setTimeout(this.topics[topic][token](data), 0);
      }
    }
  };
}
