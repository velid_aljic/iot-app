import { IError } from "utils/http";

export interface Action {
  type: string;
}

export interface IActionBody<P, M> {
  meta?: M;
  payload?: P;
  error?: IError;
}

export interface IAction<P, M> extends Action, IActionBody<P, M> {}

export type IActionCreator<P, M> = (params?: IActionBody<P, M>) => IAction<P, M>;

export interface IAsyncActions<P, M> extends Action {
  started: IActionCreator<P, M>;
  success: IActionCreator<P, M>;
  failure: IActionCreator<P, M>;
}
