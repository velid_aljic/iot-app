interface IUnique {
  id: number;
}

export const concatToList = <T>(list: T[], item: T) => {
  return (list as T[]).concat(item);
};

export const removeFromList = <T extends IUnique>(list: T[], item: T) => {
  return (list as T[]).filter((_item: T) => {
    return _item.id !== item.id;
  });
};
