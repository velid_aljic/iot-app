import { IAction, IActionBody } from "./types";

/**
 *
 *
 * Action creator
 * Returns action with defined type and optional payload/error
 *
 * @template P : Type assignable to payload property of IActionBody
 * @template M : Type assignable to meta property of IActionBody
 * @param type : action type
 * @param params : action parameters
 *
 * @example actionCreator<IResource>("FETCH_RESOURCE") -> f(params? IActionBody<IResource>)
 * @example actionCreator<IResource>("FETCH_RESOURCE")([IActionBody<IResource>]) -> {type: "FETCH_RESOURCE", payload:..., error:...}
 */
export const actionCreator = <P, M>(type: string) => (params?: IActionBody<P, M>): IAction<P, M> => ({
  type,
  ...params,
});

/**
 *
 *
 * Async action creator
 * Returns object with defined type and three action creators : (started, success, failure).
 *
 * @template P : Type assignable to payload property of IActionBody
 * @template M : Type assignable to meta property of IActionBody
 * @param type : action type
 *
 * @example asyncActionCreator<IResource>("FETCH_RESOURCE") -> {type:"FETCH_RESOURCE", started: f([IActionBody<IResource>]) ...}
 * @example _.started({payload:...}) -> {type: "FETCH_RESOURCE_STARTED", payload: ...}
 */
export const asyncActionCreator = <P, M>(type: string) => {
  return {
    type,
    started: actionCreator<P, M>(`${type}_STARTED`),
    success: actionCreator<P, M>(`${type}_SUCCESS`),
    failure: actionCreator<P, M>(`${type}_FAILURE`),
  };
};
