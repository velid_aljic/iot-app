import { Dispatch } from "redux";

import { IError, IResponse } from "utils/http";

import { IAction, IAsyncActions } from "./types";

/**
 *
 *
 * DISPATCHER CREATOR
 * Dispatches action to store
 *
 * @template P : Type assignable to payload property of IActionBody
 * @template M : Type assignable to meta property of IActionBody
 * @param action : action for dispatch
 *
 * @example _action = actionCreator("FETCH_RESOURCE")({payload: ...})
 * @example dispatcherCreator(_action) -> f(dispatcher: Dispatcher) -> [dispached]
 */
export const dispatcherCreator = <P, M>(action: IAction<P, M>) => (dispatch: Dispatch) => {
  dispatch(action);
};

/**
 *
 *
 * ASYNCHRONOUS DISPATCHER CREATOR
 * Resolves asynchronous workflow and accordingly dispatches actions to store
 *
 * @template P : Type expected as result of asynchronous function
 * @template M : Type provided to asynchronous function
 * @param actions : actions for dispatch
 * @param asyncJob: asynchronous function
 * @param params : parameters assignable to asyncJob function
 *
 * @example _asyncActions = asyncActionCreator<IResource>("FETCH_RESOURCE")
 * @example asyncDispatcherCreator<IResource, IFetchResourceParams>(_asyncActions, fetchResource) -> f(params: IFetchResourceParams)
 * @example f(dispatcher: Dispatcher) -> [dispached]
 */
export const asyncDispatcherCreator = <P, M>(
  actions: IAsyncActions<P, M>,
  asyncJob: (params?: M) => Promise<IResponse<P>>,
) => (meta?: M) => {
  return async (dispatch: Dispatch): Promise<P> => {
    dispatch(actions.started({ meta }));

    try {
      const { content } = await asyncJob(meta);
      dispatch(actions.success({ meta, payload: content }));
      return Promise.resolve(content as P);
    } catch (error) {
      dispatch(actions.failure({ error, meta }));
      throw error;
    }
  };
};

/**
 *
 *
 * ASYNCHRONOUS DISPATCHER ADAPTER
 * Enables invoking asyncDispatherCreator method with parameters provided in different order.
 * Usefull when maping dispatchers to props.
 * Parameters and generic types are equivalent to those of asyncDispatcherCreator function.
 */
export const asyncDispatcherAdapter = <P, M>(
  actions: IAsyncActions<P, M>,
  asyncJob: (meta?: M) => Promise<IResponse<P>>,
) => (dispatch: Dispatch) => (meta?: M) => {
  return asyncDispatcherCreator(actions, asyncJob)(meta)(dispatch);
};

/**
 *
 *
 *
 * DEPRECATED METHODS
 */

export const ASYNC_DISPATCHER_DEPRECATED = <T, M>(
  actions: IAsyncActions<T, M>,
  asyncJob: (meta?: M) => Promise<T | void>,
  secondaryCallback?: (secondaryParam?: any) => void,
) => (meta?: M) => {
  return async (dispatch: Dispatch): Promise<T | void> => {
    dispatch(actions.started());
    asyncJob(meta)
      .then(res => {
        if (secondaryCallback) {
          secondaryCallback(res);
        }
        dispatch(actions.success());
        return Promise.resolve();
      })
      .catch((error: IError) => {
        dispatch(actions.failure({ error }));
        return Promise.reject();
      });
  };
};

export const ASYNC_DISPATCHER_ADAPTER_DEPRECATED = <T, M>(
  actions: IAsyncActions<T, M>,
  asyncJob: (meta?: M) => Promise<T | void>,
  secondaryCallback?: (secondaryParam?: any) => void,
) => (dispatch: Dispatch) => (meta?: M) => {
  return ASYNC_DISPATCHER_DEPRECATED(actions, asyncJob, secondaryCallback)(meta)(dispatch);
};
