export interface IUrlParams {
  [key: string]: any;
}

export interface IURLSortConfig extends IUrlParams {
  sortBy: string;
  order: "DESC" | "ASC";
}

export interface IURLPageConfig extends IUrlParams {
  page: number;
  size: number;
}

export interface IURLFilterConfig extends IUrlParams {}

export interface IURLPathParams extends IUrlParams {}

export interface IURLSortParams {
  sortConfig?: IURLSortConfig[];
  sortScope?: string;
}

export interface IURLSearchParams extends IUrlParams, IURLSortParams, Partial<IURLPageConfig> {
  filtersConfig?: IURLFilterConfig;
}
