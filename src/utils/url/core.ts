import isArray from "lodash/isArray";
import isNil from "lodash/isNil";
import isUndefined from "lodash/isUndefined";

import { IURLPathParams, IURLSearchParams } from "./types";

export const buildParamsArray = ({ ...params }: IURLSearchParams = {}) => {
  const query = [];

  const prepareKeyValue = (key: string, value: any) => {
    return [key, isNil(value) ? "" : encodeURIComponent(value)];
  };

  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      const value = params[key];

      if (isArray(value)) {
        for (const partialQuery of value) {
          query.push(prepareKeyValue(key, partialQuery));
        }
      } else if (!isUndefined(value)) {
        query.push(prepareKeyValue(key, value));
      }
    }
  }

  return query;
};

export const pathBuilder = (compiler: (path: string, params: IURLSearchParams) => string) => (
  path: string = "",
  params: IURLPathParams = {},
) => compiler(path, params);
