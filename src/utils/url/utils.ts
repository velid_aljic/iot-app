export const getLastSegmentOfUrl = (url: string = "") => {
  const splitedUrl = url.split("/");
  const notEmptySegments = splitedUrl.filter((segment: string) => !!segment);
  return notEmptySegments[notEmptySegments.length - 1];
};
