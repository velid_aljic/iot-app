import isEmpty from "lodash/isEmpty";

import { buildParamsArray, pathBuilder } from "./core";
import { IURLPathParams, IURLSearchParams, IURLSortConfig } from "./types";

const DEFAULT_SORT_PARAMS_PATTERN = ({ sortBy, order }: IURLSortConfig) => `${sortBy},${order}`;

const compiler = (path: string, params: IURLSearchParams) => {
  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      const val = params[key];
      path = path.replace(`{${key}}`, val.toString());
    }
  }

  return path;
};

export const pathFrom = pathBuilder(compiler);

export const searchFrom = (
  { page, size, filtersConfig = {}, sortConfig = [], ...rest }: IURLSearchParams = {},
  sortPattern = DEFAULT_SORT_PARAMS_PATTERN,
) => {
  const params = buildParamsArray({ page, size, ...filtersConfig, ...rest }).map((paramArray: [string, string]) =>
    paramArray.filter(e => !!e).join("="),
  );

  const sortArray = buildParamsArray({ sort: sortConfig.map((s: IURLSortConfig) => sortPattern(s)) }).map(
    (paramArray: [string, string]) => paramArray.join("="),
  );

  return params.concat(sortArray).join("&");
};

export const compose = (path: string = "", pathParams: IURLPathParams = {}, search: IURLSearchParams = {}) => {
  return `${pathFrom(path, pathParams)}${isEmpty(search) ? "" : `?${searchFrom(search)}`}`;
};
