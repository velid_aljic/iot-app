import { IRoute } from "utils/routes";

export const filterSidebar = (route: IRoute) => route.inSidebar;
export const filterHome = (route: IRoute) => route.inSidebar && route.name !== "home";

export const rankSorter = (a: IRoute, b: IRoute) => {
  if (a.rank === b.rank) {
    return 0;
  }
  if (a.rank === undefined) {
    return -1;
  }
  if (b.rank === undefined) {
    return 1;
  }
  return a.rank - b.rank;
};
