import { compile } from "path-to-regexp";
import { matchPath } from "react-router";

import { IURLPathParams, IURLSearchParams, pathBuilder } from "utils/url";

import { IRoute, IRouter } from "./types";

// compatibility with react router paths
const compilerCache = new Map();
const compiler = (path: string, params: IURLSearchParams) => {
  try {
    if (compilerCache.has(path)) {
      return compilerCache.get(path)(params);
    }

    const generator = compile(path);
    compilerCache.set(path, generator);
    return generator(params);
  } catch (error) {
    if (error instanceof TypeError) {
      return path;
    }

    throw error;
  }
};

export const pathFrom = pathBuilder(compiler);

export const routerFrom = (routes: IRoute[]) => {
  const router = {} as IRouter;

  Object.defineProperty(router, "entries", { value: new Map() });

  Object.defineProperty(router, "get", {
    get: () => (name: string, params?: IURLPathParams) => {
      const route = router.entries.get(name);
      if (route) {
        if (params) {
          return { ...route, path: pathFrom(route.path, params) };
        }
        return route;
      } else {
        throw new Error(`Route with name "${name}" not found, please check config files.`);
      }
    },
  });

  routes.forEach(route => {
    Object.freeze(route);
    router.entries.set(route.name, route);
    Object.defineProperty(router, route.name, {
      enumerable: true,
      value: router.entries.get(route.name),
      writable: false,
    });
  });

  return router;
};

export const routerWith = (routers: IRouter[]) => {
  const routes: IRoute[] = [];
  routers.forEach(route => {
    routes.push(...Object.values(route));
  });

  return routerFrom(routes);
};

const findExactRoute = (pathname: string, routes: IRoute[]) => {
  return routes.find(route => !!route.exact && !!matchPath(pathname, { path: route.path, strict: true, exact: true }));
};

const findMatchRoute = (pathname: string, routes: IRoute[]) => {
  return routes.find(route => {
    return !!matchPath(pathname, { path: route.path, strict: true, exact: true });
  });
};

export const routeByPath = (pathname: string, router: IRouter) => {
  const routes = Object.values(router);
  return findExactRoute(pathname, routes) || findMatchRoute(pathname, routes);
};

export const routeParents = (route: IRoute, router: IRouter) => {
  const results: IRoute[] = [];
  if (route.name) {
    const name = route.name.split(".");
    if (name.length > 1) {
      const next = router.entries.get(name.slice(0, -1).join("."));
      if (next) {
        results.unshift(next);
        results.unshift(...routeParents(next, router));
        return results;
      }
    }
  }

  return results;
};
