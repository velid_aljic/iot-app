import { SemanticICONS } from "semantic-ui-react";

import { IURLPathParams } from "utils/url";

export interface IRoute {
  exact?: boolean;
  label?: string;
  name: string;
  path: string;
  inSidebar?: boolean;
  rank?: number;
  icon?: SemanticICONS;
}

interface RouterMethods {
  readonly get: (name: string, params?: IURLPathParams) => IRoute;
  readonly entries: Map<string, IRoute>;
}

interface RouterSignature extends Iterable<IRoute> {
  readonly [key: string]: IRoute;
}

export type IRouter = RouterSignature & RouterMethods & {};
