import { SemanticICONS } from "semantic-ui-react";

import { routerFrom } from "utils/routes";

export const routes = [
  {
    rank: 1,
    exact: true,
    inSidebar: true,
    label: "Dashboard",
    name: "dashboards-list",
    icon: "dashboard" as SemanticICONS,
    path: "/dashboards",
  },
  {
    exact: true,
    label: "Dashboard Details",
    name: "dashboard-details",
    path: "/dashboards/:id",
  },
];

export const router = routerFrom(routes);
