import moment from "moment";
import React from "react";
import Loader from "react-loader";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Breadcrumb, Button, Card, Grid, Icon, Menu, Pagination, Popup, Search } from "semantic-ui-react";

import { Pushable } from "components/Pushable";

import {
  createDashboard,
  EDashboard,
  EDashboards,
  fetchDashboards,
  IDashboard,
  IDashboards,
  IPostDashboard,
} from "data/dashboard";

import { ContextInfo, ContextMenu } from "views/Session/components/ContextPortal";

import { IRootStore } from "store";

import { CreateDashboardConnectedForm } from "../components/forms";

/**
 *
 *
 * INTERFACES
 */

// PROPS

interface StateProps {
  isWorking: boolean;
  dashboards: IDashboards;
}

interface DispatchProps {
  fetchDashboards: () => Promise<EDashboards>;
  createDashboard: (data: IPostDashboard) => Promise<EDashboard>;
}

interface ComponentProps extends RouteComponentProps {}

interface Props extends ComponentProps, DispatchProps, StateProps {}

// STATE

interface State {
  openCreate: boolean;
}

// MAPPING

const mapStateToProps = (state: IRootStore) => {
  const { list, isWorking } = state.dashboards;

  return {
    isWorking,
    dashboards: list,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    fetchDashboards: fetchDashboards(dispatch),
    createDashboard: createDashboard(dispatch),
  };
};

// CLASS

class DashboardsListComponent extends React.PureComponent<Props, State> {
  state = { openCreate: false };

  // LIFECYCLE METHODS

  componentDidMount() {
    this.fetchData();
  }

  // COMPONENT METHODS

  fetchData() {
    this.props.fetchDashboards();
  }

  handleClick = (dashboardId: number) => {
    const path = `/dashboards/${dashboardId}`;
    this.props.history.push(path);
  };

  toggleCreateDashboard = () => this.setState({ openCreate: !this.state.openCreate });

  // RENDER METHODS

  renderInfo = () => {
    return (
      <Menu.Item>
        <Breadcrumb size="big">
          <Breadcrumb.Section active>Dashboards</Breadcrumb.Section>
        </Breadcrumb>
      </Menu.Item>
    );
  };

  renderMenu = () => {
    const { openCreate } = this.state;

    const create = <Button circular icon={!openCreate ? "add" : "close"} onClick={this.toggleCreateDashboard} />;
    const filter = <Button circular icon="filter" onClick={() => null} />;

    return (
      <>
        <Menu.Item className="menuSearch">
          <Search aligned="right" size="mini" placeholder="Search..." />
        </Menu.Item>
        <Menu.Item>
          <Popup position="bottom right" trigger={create} content={!openCreate ? "Create dashboard" : "Close"} />
        </Menu.Item>
        <Menu.Item>
          <Popup position="bottom right" trigger={filter} content="Filter" />
        </Menu.Item>
      </>
    );
  };

  renderDashboardCard = (dashboard: IDashboard) => {
    const { id, name, widgets, createdAt, modifiedAt } = dashboard;

    return (
      <Grid.Column stretched key={`dashboard_${name}`} textAlign="center" width={4}>
        <Card fluid link onClick={() => this.handleClick(id)}>
          <Card.Content>
            <Card.Description>{name}</Card.Description>
          </Card.Content>
          <Card.Content>
            <Card.Meta>Created at: {moment(createdAt).format("DD.MM.YYYY hh:mm")}</Card.Meta>
            <Card.Meta>Modified at: {moment(modifiedAt).format("DD.MM.YYYY hh:mm")}</Card.Meta>
          </Card.Content>
          <Card.Content extra>
            <Icon name="chart area" />
            {widgets.length} {widgets.length === 1 ? "Widget" : "Widgets"}
          </Card.Content>
        </Card>
      </Grid.Column>
    );
  };

  // RENDER

  render() {
    const { openCreate } = this.state;
    const { dashboards, isWorking } = this.props;

    const forms = <CreateDashboardConnectedForm toggle={this.toggleCreateDashboard} />;

    return (
      <>
        <ContextInfo loaded={!isWorking}>{this.renderInfo()}</ContextInfo>
        <ContextMenu loaded={!isWorking}>{this.renderMenu()}</ContextMenu>

        <Pushable open={openCreate} forms={forms}>
          <Loader scale={2} loadedClassName="" loaded={!isWorking}>
            <Grid centered padded stackable doubling columns={4}>
              {dashboards.map(this.renderDashboardCard)}
            </Grid>

            <Grid centered>
              <Pagination defaultActivePage={1} firstItem={null} lastItem={null} pointing secondary totalPages={1} />
            </Grid>
          </Loader>
        </Pushable>
      </>
    );
  }
}

// EXPORT

export const DashboardsListPage = withRouter(
  connect<StateProps, DispatchProps, ComponentProps>(
    mapStateToProps,
    mapDispatchToProps,
  )(DashboardsListComponent),
);
