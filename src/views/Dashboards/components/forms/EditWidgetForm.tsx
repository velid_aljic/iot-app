import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { reduxForm } from "redux-form";

import { EWidget, IPatchWidget, IWidget, updateWidget } from "data/dashboard";

import { mapFilterFormToInterface, mapWidgetToForm } from "./mappers";
import { IWidgetForm, validateWidgetForm, WidgetForm } from "./WidgetForm";

/**
 *
 *
 * CONSTANTS
 */

export const EDIT_WIDGET_FORM = "editWidgetForm";

/**
 *
 *
 * FORM
 */

const EditWidgetForm = WidgetForm("Edit Widget");

/**
 *
 *
 * CONNECTED FORM
 */

const EditWidgetReduxForm = reduxForm({
  form: EDIT_WIDGET_FORM,
  validate: validateWidgetForm,
})(EditWidgetForm);

/**
 *
 *
 * INTERFACE
 */

// PROPS

interface DispatchProps {
  updateWidget: (data: IPatchWidget) => Promise<EWidget>;
}

interface ComponentProps {
  dashboardId: number;
  widgetId?: number;
  widgets: IWidget[];
  toggle: (widgetId?: number) => void;
}

interface Props extends DispatchProps, ComponentProps {}

/**
 *
 *
 * MAPPING
 */

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    updateWidget: updateWidget(dispatch),
  };
};

/**
 *
 *
 * CLASS
 */

class EditWidgetComponent extends React.PureComponent<Props> {
  // COMPONENT METHODS

  // SAVE METHOD
  update = async (values: IWidgetForm) => {
    const { toggle, dashboardId, widgetId } = this.props;
    const { chartType, deviceId, name, ...rest } = values;

    try {
      await this.props.updateWidget({
        chartType,
        dashboardId,
        deviceId,
        filters: { ...mapFilterFormToInterface(rest) },
        name,
        widgetId: widgetId || 0,
      });
    } catch (response) {
      // @todo: add error handler
    } finally {
      toggle();
    }
  };

  // RENDER

  render() {
    const { widgetId, widgets } = this.props;
    return <EditWidgetReduxForm initialValues={mapWidgetToForm(widgetId || 0, widgets)} onSubmit={this.update} />;
  }
}

/**
 *
 *
 * EXPORT
 */

export const EditWidgetConnectedForm = connect<null, DispatchProps, ComponentProps>(
  null,
  mapDispatchToProps,
)(EditWidgetComponent);
