import moment from "moment";

import { IWidget } from "data/dashboard";
import { IFilter, TOrders, TSelectionAt, TSelectionValue } from "data/shared";

import { IFilterForm, IWidgetForm } from "./WidgetForm";

export const mapFilterFormToInterface = ({ groupBy, selectBy, orderBy, from, to }: IFilterForm): IFilter => {
  // Filters
  const recordedAt = Object.assign({}, !!from ? { $gt: from } : undefined, !!to ? { $lt: to } : undefined);
  const filters = { recordedAt };

  // Selections
  const selections = Object.assign(
    { recordValue: selectBy as TSelectionValue },
    selectBy === "value" ? { recordedAt: "value" as TSelectionAt } : undefined,
  );

  let groups = {};
  let orders = {};

  if (!!groupBy) {
    // Groups
    groups = {
      recordedAt: groupBy,
    };
    // Orders
    orders = {
      groupRecordedAt: orderBy,
    };
  } else {
    // Orders
    orders = {
      recordedAt: orderBy,
    };
  }

  return {
    filters,
    selections,
    groups,
    orders,
  };
};

export const mapWidgetToForm = (_id: number, _widgets: IWidget[]): IWidgetForm => {
  const widget = _widgets.find((_widget: IWidget) => {
    return _widget.id === _id;
  });

  const { deviceId, chartType, name } = widget as IWidget;
  const { filters, orders, groups, selections } = (widget as IWidget).filters as IFilter;
  const { $gt, $lt } = !!filters && !!filters.recordedAt && filters.recordedAt;

  const orderBy = !!orders.groupRecordedAt ? orders.groupRecordedAt : orders.recordedAt;
  const selectBy = selections.recordValue;
  const from = !!$gt ? moment($gt) : undefined;
  const to = !!$lt ? moment($lt) : undefined;

  return {
    name,
    deviceId,
    chartType,
    groupBy: groups.recordedAt,
    orderBy: orderBy as TOrders,
    selectBy,
    from,
    to,
  };
};
