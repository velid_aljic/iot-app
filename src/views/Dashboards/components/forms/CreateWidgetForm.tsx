import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { reduxForm } from "redux-form";

import { DEFAULT_RELATIVE_POSITION } from "components/Draggable";

import { createWidget, EWidget, IPostWidget } from "data/dashboard";

import { IRootStore } from "store";

import { mapFilterFormToInterface } from "./mappers";
import { IWidgetForm, validateWidgetForm, WidgetForm } from "./WidgetForm";

/**
 *
 *
 * CONSTANTS
 */

export const CREATE_WIDGET_FORM = "createWidgetForm";

/**
 *
 *
 * FORM
 */

const CreateWidgetForm = WidgetForm("Create New Widget");

/**
 *
 *
 * CONNECTED FORM
 */

const CreateWidgetReduxForm = reduxForm({
  form: CREATE_WIDGET_FORM,
  validate: validateWidgetForm,
})(CreateWidgetForm);

/**
 *
 *
 * INTERFACE
 */

// PROPS

interface DispatchProps {
  createWidget: (data: IPostWidget) => Promise<EWidget>;
}

interface StateProps {
  isCreatingWidget: boolean;
}

interface ComponentProps {
  dashboardId: number;
  toggle: () => void;
}

interface Props extends DispatchProps, StateProps, ComponentProps {}

/**
 *
 *
 * MAPPING
 */

const mapStateToProps = (state: IRootStore) => {
  const { isCreatingWidget } = state.dashboards;

  return {
    isCreatingWidget,
  };
};

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    createWidget: createWidget(dispatch),
  };
};

/**
 *
 *
 * CLASS
 */

class CreateWidgetComponent extends React.PureComponent<Props> {
  // COMPONENT METHODS

  // SAVE METHOD
  save = async (values: IWidgetForm) => {
    const { dashboardId, toggle } = this.props;
    const { chartType, deviceId, name, ...rest } = values;

    try {
      await this.props.createWidget({
        chartType,
        dashboardId,
        deviceId,
        name,
        filters: { ...mapFilterFormToInterface(rest) },
        ...DEFAULT_RELATIVE_POSITION,
      });
    } catch (response) {
      // @todo: add error handler
    } finally {
      toggle();
    }
  };

  // RENDER

  render() {
    return <CreateWidgetReduxForm onSubmit={this.save} />;
  }
}

/**
 *
 *
 * EXPORT
 */

export const CreateWidgetConnectedForm = connect<StateProps, DispatchProps, ComponentProps>(
  mapStateToProps,
  mapDispatchToProps,
)(CreateWidgetComponent);
