import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Field, reduxForm } from "redux-form";
import { Button, Divider, Form, Header, Icon } from "semantic-ui-react";

import { Input, required } from "components/Form";

import { createDashboard, EDashboard, IPostDashboard } from "data/dashboard";

import { IRootStore } from "store";

/**
 *
 *
 * CONSTANTS
 */

export const CREATE_DASHBOARD_FORM = "createDashboardForm";

/**
 *
 *
 * INTERFACE
 */

export interface ICreateDashboardForm {
  name: string;
}

/**
 *
 *
 * FORM
 */

// @TODO: ADD PROP TYPES
const CreateDashboardForm = (props: any) => {
  const { handleSubmit, reset, pristine, submitting } = props;

  return (
    <>
      <Header content="Create New Dashboard" icon="dashboard" />

      <Divider />

      <Form onSubmit={handleSubmit}>
        <Form.Group>
          <Field
            name="name"
            label="Dashboard name"
            type="text"
            placeholder="eg. My Dashboard"
            width={16}
            required={true}
            validate={[required]}
            component={Input}
          />
        </Form.Group>

        <Divider />

        <Form.Group>
          <Button floated="right" icon disabled={pristine || submitting} onClick={reset}>
            <Icon name="refresh" size="large" />
          </Button>
          <Button floated="right" icon disabled={pristine || submitting}>
            <Icon name="save" size="large" />
          </Button>
        </Form.Group>
      </Form>
    </>
  );
};

/**
 *
 *
 * CONNECTED FORM
 */

const CreateDashboardReduxForm = reduxForm({
  form: CREATE_DASHBOARD_FORM,
})(CreateDashboardForm);

/**
 *
 *
 * CONNECTED COMPONENT
 */

// PROPS

interface DispatchProps {
  createDashboard: (data: IPostDashboard) => Promise<EDashboard>;
}

interface StateProps {
  isWorking: boolean;
}

interface ComponentProps extends RouteComponentProps {
  toggle: () => void;
}

interface Props extends DispatchProps, StateProps, ComponentProps {}

/**
 *
 *
 * MAPPING
 */

const mapStateToProps = (state: IRootStore) => {
  const { isWorking } = state.dashboards;

  return {
    isWorking,
  };
};

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    createDashboard: createDashboard(dispatch),
  };
};

/**
 *
 *
 * CLASS
 */

class CreateDashboardComponent extends React.PureComponent<Props> {
  // COMPONENT METHODS

  save = async (values: ICreateDashboardForm) => {
    try {
      const dashboard = await this.props.createDashboard({ ...values });
      this.props.history.push(`/dashboards/${dashboard.id}`);
    } catch (response) {
      // @todo: add error handler
    } finally {
      this.props.toggle();
    }
  };

  // RENDER

  render() {
    return <CreateDashboardReduxForm onSubmit={this.save} />;
  }
}

/**
 *
 *
 * EXPORT
 */

export const CreateDashboardConnectedForm = withRouter(
  connect<StateProps, DispatchProps, ComponentProps>(
    mapStateToProps,
    mapDispatchToProps,
  )(CreateDashboardComponent),
);
