import { Moment } from "moment";
import React from "react";
import { Field } from "redux-form";

import { Button, Divider, Form, Header, Icon } from "semantic-ui-react";

import { DatePicker, Dropdown, Input, required } from "components/Form";

import { chartTypes, groupByTypes, orderTypes, selectionTypes, TOrders } from "data/shared";

/**
 *
 *
 * INTERFACE
 */

export interface IFilterForm {
  from?: Moment;
  groupBy?: string;
  orderBy: TOrders;
  selectBy: string;
  to?: Moment;
}

export interface IWidgetForm extends IFilterForm {
  chartType: string;
  deviceId: number;
  name: string;
}

/**
 *
 *
 * SYNC VALIDATOR
 */

export const validateWidgetForm = (values: IWidgetForm) => {
  let errors = {};

  if (values.selectBy === "value" && !!values.groupBy) {
    errors = Object.assign(errors, { groupBy: "Group by is not allowed when selection is done by value" });
  }

  if (values.selectBy !== "value" && !values.groupBy) {
    errors = Object.assign(errors, { groupBy: "Group by is required when selection is not done by value" });
  }

  return errors;
};

/**
 *
 *
 * WIDGET FORM
 */

// @TODO: ADD PROP TYPES
export const WidgetForm = (header: string) => (props: any) => {
  const { handleSubmit, reset, pristine, submitting } = props;

  return (
    <>
      <Header content={header} icon="area chart" />

      <Divider />

      <Form onSubmit={handleSubmit}>
        <Form.Group>
          <Field name="name" label="Name" width={16} required={true} validate={[required]} component={Input} />
        </Form.Group>

        <Form.Group>
          <Field
            name="deviceId"
            label="Device id"
            type="number"
            width={16}
            required={true}
            validate={[required]}
            component={Input}
          />
        </Form.Group>

        <Form.Group>
          <Field
            name="chartType"
            label="Chart type"
            width={16}
            validate={[required]}
            required={true}
            component={Dropdown}
            options={chartTypes}
          />
        </Form.Group>

        <Form.Group>
          <Field
            name="selectBy"
            label="Select by"
            width={16}
            validate={[required]}
            required={true}
            component={Dropdown}
            options={selectionTypes}
          />
        </Form.Group>

        <Form.Group>
          <Field name="groupBy" label="Group by" width={16} component={Dropdown} options={groupByTypes} />
        </Form.Group>

        <Form.Group>
          <Field
            name="orderBy"
            label="Order by"
            width={16}
            validate={[required]}
            required={true}
            component={Dropdown}
            options={orderTypes}
          />
        </Form.Group>

        <Form.Group>
          <Field name="from" label="From" width={8} component={DatePicker} />
          <Field name="to" label="To" width={8} component={DatePicker} />
        </Form.Group>

        <Divider />

        <Form.Group>
          <Button floated="right" icon disabled={pristine || submitting} onClick={reset}>
            <Icon name="refresh" size="large" />
          </Button>
          <Button floated="right" icon disabled={pristine || submitting}>
            <Icon name="save" size="large" />
          </Button>
        </Form.Group>
      </Form>
    </>
  );
};
