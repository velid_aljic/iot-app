import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { EWidget, IPatchWidget, patchWidget } from "data/dashboard";
import { IPosition } from "data/shared";

import { RndComponent } from "components/Draggable";

/**
 *
 *
 * Connects Draggable component to dashboards widget property
 * Properties required for widget update: widget_id && dashboard_id
 */

// INTERFACES

interface IUpdateProps {
  widgetId: number;
  dashboardId: number;
}

interface ComponentProps extends IPosition, IUpdateProps {}

interface DispatchProps {
  update: (params: IPatchWidget) => Promise<EWidget>;
}

interface Props extends ComponentProps, DispatchProps {}

interface State extends IPosition {}

// MAPPING

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    update: patchWidget(dispatch),
  };
};

// CLASS

export class DraggableComponent extends React.PureComponent<Props, State> {
  // RENDER

  render() {
    const { update, children, width, height, x, y, ...params } = this.props;
    const position = { width, height, x, y };

    return <RndComponent update={update} children={children} params={params} {...position} />;
  }
}

// EXPORT

export const Draggable = connect<null, DispatchProps, ComponentProps>(
  null,
  mapDispatchToProps,
)(DraggableComponent);
