import React from "react";

import { IWidget } from "data/dashboard";

import { Draggable } from "./Draggable";
import { Widget } from "./Widget";

/**
 *
 *
 * Renders Widget as draggable
 */

interface Props {
  menuClassName: string;
  dashboardId: number;
  widget: IWidget;
  toggleEdit: (widgetId?: number) => void;
}

export class RndWidget extends React.PureComponent<Props> {
  render() {
    const { dashboardId, widget, menuClassName, toggleEdit } = this.props;
    const { id, height, width, x, y } = widget;
    const position = { height, width, x, y };

    return (
      <Draggable dashboardId={dashboardId} widgetId={id} {...position}>
        <Widget menuClassName={menuClassName} dashboardId={dashboardId} widget={widget} toggleEdit={toggleEdit} />
      </Draggable>
    );
  }
}
