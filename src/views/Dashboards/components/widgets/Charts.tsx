import { isNil } from "lodash";
import moment, { Moment } from "moment";
import React from "react";

import { AreaChart, BarChart, ChartContainer, LineChart } from "components/Charts";
import { PositionContext, toNumber } from "components/Draggable";

import { IWidget } from "data/dashboard";
import { IRecording } from "data/recordings";
import { IPosition } from "data/shared";

interface Props {
  widget: IWidget;
}

export class Charts extends React.PureComponent<Props> {
  render() {
    const { data, chartType } = this.props.widget;

    const xAxis = isNil(data) || isNil(data[0]) || isNil(data[0].groupRecordedAt) ? "recordedAt" : "groupRecordedAt";

    // @todo: REFACTOR!
    const chartData =
      xAxis === "recordedAt"
        ? data.map((item: IRecording) => {
            return {
              ...item,
              recordedAt: (moment(item.recordedAt).format("YYYY-MM-DD hh:mm") as unknown) as Moment,
            };
          })
        : data;

    // @todo: REFACTOR!

    switch (chartType) {
      case "line":
        return (
          <PositionContext.Consumer>
            {(position: IPosition) => {
              return (
                <ChartContainer height={toNumber(position.height)}>
                  <LineChart
                    height={toNumber(position.height)}
                    width={toNumber(position.width)}
                    dataKey="recordValue"
                    xAxis={xAxis}
                    data={chartData}
                  />
                </ChartContainer>
              );
            }}
          </PositionContext.Consumer>
        );

      case "area":
        return (
          <PositionContext.Consumer>
            {(position: IPosition) => {
              return (
                <ChartContainer height={toNumber(position.height)}>
                  <AreaChart
                    height={toNumber(position.height)}
                    width={toNumber(position.width)}
                    dataKey="recordValue"
                    xAxis={xAxis}
                    data={chartData}
                  />
                </ChartContainer>
              );
            }}
          </PositionContext.Consumer>
        );

      case "bar":
        return (
          <PositionContext.Consumer>
            {(position: IPosition) => {
              return (
                <ChartContainer height={toNumber(position.height)}>
                  <BarChart
                    height={toNumber(position.height)}
                    width={toNumber(position.width)}
                    dataKey="recordValue"
                    xAxis={xAxis}
                    data={chartData}
                  />
                </ChartContainer>
              );
            }}
          </PositionContext.Consumer>
        );

      default:
        return <h1>Chart Not Supported</h1>;
    }
  }
}
