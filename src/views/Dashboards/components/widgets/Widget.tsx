import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { Widget as WidgetBase } from "components/Widgets";

import { deleteWidget, EWidget, IPatchWidget, IRemoveWidget, IWidget, patchWidget } from "data/dashboard";
import { ERecordings, fetchRecordings, IGetRecordings } from "data/recordings";

import { DeleteWidgetModal } from "../modals";
import { Charts } from "./Charts";

/**
 *
 *
 * Connects Widget component to application
 */

// INTERFACES

interface ComponentProps {
  dashboardId: number;
  menuClassName: string;
  widget: IWidget;
  toggleEdit: (widgetId?: number) => void;
}

interface DispatchProps {
  deleteWidget: (params: IRemoveWidget) => Promise<EWidget>;
  fetchRecordings: (params: IGetRecordings) => Promise<ERecordings>;
  updateWidget: (params: IPatchWidget) => Promise<EWidget>;
}

interface Props extends ComponentProps, DispatchProps {}

interface State {
  openDelete: boolean;
}

// MAPPING

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    deleteWidget: deleteWidget(dispatch),
    fetchRecordings: fetchRecordings(dispatch),
    updateWidget: patchWidget(dispatch),
  };
};

// CLASS

export class WidgetComponent extends React.Component<Props, State> {
  // CONSTRUCTOR

  constructor(props: Props) {
    super(props);

    this.state = {
      openDelete: false,
    };
  }

  // COMPONENT METHODS

  toggleDeleteModal = () => {
    this.setState({ openDelete: !this.state.openDelete });
  };

  // RENDER

  render() {
    const { dashboardId, widget, menuClassName } = this.props;
    const { openDelete } = this.state;
    const { id, deviceId, filters } = widget;

    // IDENTITIES

    const identities = {
      dashboardId,
      deviceId,
      filters,
      widgetId: id,
    };

    const updateId = ["dashboardId", "widgetId"];
    const fetchId = ["deviceId", "widgetId", "filters"];

    return (
      <>
        {openDelete && (
          <DeleteWidgetModal
            dashboardId={dashboardId}
            widgetId={widget.id}
            isWorking={widget.isWorking}
            remove={this.props.deleteWidget}
            toggle={this.toggleDeleteModal}
          />
        )}

        <WidgetBase<IPatchWidget, IGetRecordings, EWidget, ERecordings>
          widget={widget}
          menuClassName={menuClassName}
          identites={identities}
          updateId={updateId}
          fetchId={fetchId}
          updateWidget={this.props.updateWidget}
          fetchRecordings={this.props.fetchRecordings}
          toggleEdit={this.props.toggleEdit}
          toggleDelete={this.toggleDeleteModal}>
          <Charts widget={widget} />
        </WidgetBase>
      </>
    );
  }
}

// EXPORT

export const Widget = connect<null, DispatchProps, ComponentProps>(
  null,
  mapDispatchToProps,
)(WidgetComponent);
