import React from "react";
import { Button, Header, Icon, Modal } from "semantic-ui-react";

import { CANCEL } from "components/Draggable";

import { EWidget, IRemoveWidget } from "data/dashboard";

/**
 *
 *
 * INTERFACE
 */

interface Props {
  widgetId: number;
  dashboardId: number;
  isWorking: boolean;
  toggle: () => void;
  remove: (params: IRemoveWidget) => Promise<EWidget>;
}

/**
 *
 *
 * CLASS
 */

export class DeleteWidgetModal extends React.PureComponent<Props> {
  // COMPONENT METHODS

  remove = async () => {
    const { dashboardId, widgetId, toggle } = this.props;
    try {
      await this.props.remove({ dashboardId, widgetId });
    } catch (response) {
      // @todo: add error handler
    } finally {
      toggle();
    }
  };

  // RENDER

  render() {
    const { isWorking, toggle } = this.props;

    return (
      <Modal open closeIcon dimmer="blurring" className={CANCEL} onClose={toggle} closeOnDimmerClick>
        <Header icon="area chart" content="Delete Widget" />

        <>
          <Modal.Content>
            <p>Are you sure you want to delete selected widget?</p>
          </Modal.Content>
        </>
        <>
          <Modal.Actions>
            <Button icon onClick={toggle}>
              <Icon name="remove" size="big" />
            </Button>
            <Button icon loading={isWorking} onClick={this.remove}>
              <Icon name="trash" size="big" />
            </Button>
          </Modal.Actions>
        </>
      </Modal>
    );
  }
}
