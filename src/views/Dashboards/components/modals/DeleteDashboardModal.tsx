import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Button, Header, Icon, Modal } from "semantic-ui-react";

import { CANCEL } from "components/Draggable";

import { deleteDashboard, EDashboard, IRemoveDashboard } from "data/dashboard";

import { IRootStore } from "store";

/**
 *
 *
 * INTERFACE
 */

// PROPS

interface DispatchProps {
  deleteDashboard: (params: IRemoveDashboard) => Promise<EDashboard>;
}

interface StateProps {
  isWorking: boolean;
}

interface MatchParams {
  id: string;
}

interface ComponentProps extends RouteComponentProps<MatchParams> {
  id: number;
  toggle: () => void;
}

interface Props extends DispatchProps, StateProps, ComponentProps {}

/**
 *
 *
 * MAPPING
 */

const mapStateToProps = (state: IRootStore) => {
  const { isWorking } = state.devices;

  return {
    isWorking,
  };
};

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    deleteDashboard: deleteDashboard(dispatch),
  };
};

// CLASS

export class DeleteDashboardComponent extends React.PureComponent<Props> {
  // COMPONENT METHODS

  remove = async () => {
    const { id } = this.props;
    try {
      await this.props.deleteDashboard({ id });
      this.props.history.replace("/dashboards");
    } catch (response) {
      // @todo: add error handler
    }
  };

  // RENDER

  render() {
    const { isWorking, toggle } = this.props;

    return (
      <Modal open closeIcon dimmer="blurring" className={CANCEL} onClose={toggle} closeOnDimmerClick>
        <Header icon="dashboard" content="Delete Dashboard" />

        <>
          <Modal.Content>
            <p>Are you sure you want to delete selected dashboard?</p>
          </Modal.Content>
        </>
        <>
          <Modal.Actions>
            <Button icon onClick={toggle}>
              <Icon name="remove" size="big" />
            </Button>
            <Button icon loading={isWorking} onClick={this.remove}>
              <Icon name="trash" size="big" />
            </Button>
          </Modal.Actions>
        </>
      </Modal>
    );
  }
}

/**
 *
 *
 * EXPORT
 */

export const DeleteDashboardModal = withRouter(
  connect<StateProps, DispatchProps, ComponentProps>(
    mapStateToProps,
    mapDispatchToProps,
  )(DeleteDashboardComponent),
);
