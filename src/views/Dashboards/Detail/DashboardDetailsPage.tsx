import React from "react";
import Loader from "react-loader";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Breadcrumb, Button, Menu, Popup } from "semantic-ui-react";

import ErrorBus from "components/Application/Errors";
import { BOUNDS, CANCEL } from "components/Draggable";
import { Pushable } from "components/Pushable";

import { EDashboard, fetchDashboard, IDashboard, IGetDashboard, IWidget } from "data/dashboard";

import { ContextInfo, ContextMenu } from "views/Session/components/ContextPortal";

import { IRootStore } from "store";

import { CreateWidgetConnectedForm, EditWidgetConnectedForm } from "../components/forms";
import { DeleteDashboardModal } from "../components/modals";
import { RndWidget } from "../components/widgets";

/**
 *
 *
 * INTERFACES
 */

// PROPS

interface StateProps {
  dashboard: IDashboard;
  isWorking: boolean;
}

interface DispatchProps {
  fetchDashboard: (params: IGetDashboard) => Promise<EDashboard>;
}

interface MatchParams {
  id: string;
}

interface ComponentProps extends RouteComponentProps<MatchParams> {}

interface Props extends ComponentProps, DispatchProps, StateProps {}

// STATE

interface State {
  currentWidgetId?: number;
  openCreate: boolean;
  openDelete: boolean;
  openEdit: boolean;
}

/**
 *
 *
 * MAPING
 */

const mapStateToProps = (state: IRootStore) => {
  const { item, isWorking } = state.dashboards;

  return {
    dashboard: item,
    isWorking,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    fetchDashboard: fetchDashboard(dispatch),
  };
};

/**
 *
 *
 * CLASS
 */

export class DashboardDetailsComponent extends React.PureComponent<Props, State> {
  state = { currentWidgetId: undefined, openCreate: false, openDelete: false, openEdit: false };

  /**
   *
   *
   * LIFECYCLE METHODS
   */

  componentDidMount() {
    const dashboardId = Number(this.props.match.params.id);

    if (Number.isNaN(dashboardId)) {
      const ERR_MSG = "Invalid parameter provided";
      return ErrorBus.publish("APP/UNCAUGHT", new Error(ERR_MSG));
    }

    this.props.fetchDashboard({ id: dashboardId });
  }

  /**
   *
   *
   * COMPONENT METHODS
   */

  toggleDeleteDashboard = () => {
    this.setState({ openDelete: !this.state.openDelete });
  };

  toggleCreateWidget = () => {
    const { openCreate, openEdit } = this.state;
    return openEdit && !openCreate
      ? this.setState({ openEdit: false })
      : this.setState({ openCreate: !this.state.openCreate, openEdit: false });
  };

  toggleEditWidget = (widgetId: number) => {
    this.setState({ openEdit: !this.state.openEdit, openCreate: false, currentWidgetId: widgetId });
  };

  /**
   *
   *
   * RENDER METHODS
   */

  renderInfo = () => {
    return (
      <Menu.Item>
        <Breadcrumb size="big">
          <Breadcrumb.Section link onClick={() => this.props.history.push("/dashboards")}>
            Dashboards
          </Breadcrumb.Section>
          <Breadcrumb.Divider icon="right angle" />
          <Breadcrumb.Section active>{this.props.dashboard.name}</Breadcrumb.Section>
        </Breadcrumb>
      </Menu.Item>
    );
  };

  renderMenu = () => {
    const { openCreate, openEdit } = this.state;

    const create = (
      <Button circular icon={openCreate || openEdit ? "close" : "add"} onClick={this.toggleCreateWidget} />
    );
    const remove = <Button circular icon="trash" onClick={this.toggleDeleteDashboard} />;

    return (
      <>
        <Menu.Item>
          <Popup
            position="bottom right"
            content={openCreate || openEdit ? "Close" : "Create widget"}
            trigger={create}
          />
        </Menu.Item>
        <Menu.Item>
          <Popup position="bottom right" trigger={remove} content="Delete dashboard" />
        </Menu.Item>
      </>
    );
  };

  renderDashboard = (dashboardId: number, widgets: IWidget[]) => {
    return widgets.map((widget: IWidget) => {
      return (
        <RndWidget
          key={`widget_${widget.id}`}
          dashboardId={dashboardId}
          widget={widget}
          menuClassName={CANCEL}
          toggleEdit={this.toggleEditWidget}
        />
      );
    });
  };

  /**
   *
   *
   * RENDER
   */

  render() {
    const { currentWidgetId, openCreate, openDelete, openEdit } = this.state;
    const { dashboard, isWorking } = this.props;
    const { id, widgets } = dashboard;

    const forms = (
      <>
        {openCreate && <CreateWidgetConnectedForm dashboardId={id} toggle={this.toggleCreateWidget} />}
        {openEdit && (
          <EditWidgetConnectedForm
            widgetId={currentWidgetId}
            dashboardId={id}
            widgets={widgets}
            toggle={this.toggleEditWidget}
          />
        )}
      </>
    );

    return (
      <>
        {openDelete && <DeleteDashboardModal id={id} toggle={this.toggleDeleteDashboard} />}

        <ContextInfo loaded={!isWorking}>{this.renderInfo()}</ContextInfo>
        <ContextMenu loaded={!isWorking}>{this.renderMenu()}</ContextMenu>

        <Pushable open={openCreate || openEdit} forms={forms}>
          <Loader scale={2} loadedClassName={BOUNDS} loaded={!isWorking}>
            {!!widgets && this.renderDashboard(id, widgets)}
          </Loader>
        </Pushable>
      </>
    );
  }
}

/**
 *
 *
 * EXPORT
 */

export const DashboardDetailsPage = withRouter(
  connect<StateProps, DispatchProps, ComponentProps>(
    mapStateToProps,
    mapDispatchToProps,
  )(DashboardDetailsComponent),
);
