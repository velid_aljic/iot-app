import { withNavigationLayout } from "views/Session/components/NavigationLayout";

import { DashboardDetailsPage } from "./Detail";
import { DashboardsListPage } from "./List";

export const pages = {
  "dashboard-details": withNavigationLayout(DashboardDetailsPage),
  "dashboards-list": withNavigationLayout(DashboardsListPage),
};
