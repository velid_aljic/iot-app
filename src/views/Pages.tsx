import React from "react";
import Loader from "react-loader";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import { Dispatch } from "redux";

import { AuthenticationAssurance } from "components/Application/Session";

import { asyncVerifyToken } from "data/session";

import { IRoute } from "utils/routes";

import { routes as applicationRoutes } from "views/router";
import { routes as sessionRoutes } from "views/Session/router";
import { pages as applicationPages } from "views/views";

import { IRootStore } from "store";

// INTERFACES

interface Props extends RouteComponentProps {
  pristine: boolean;
  validateToken: () => void;
}

// MAPPING

const mapStateToProps = (state: IRootStore) => {
  const { pristine } = state.session;

  return {
    pristine,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    validateToken: asyncVerifyToken(dispatch),
  };
};

// CLASS

class PagesComponent extends React.Component<Props> {
  // LIFECYCLE METHODS

  componentDidMount() {
    this.props.validateToken();
  }

  // RENDER METHODS

  renderRoute(route: IRoute, index: number) {
    const { exact, path, name } = route;

    return (
      <Route
        key={index}
        exact={exact}
        path={path}
        render={(props: any) => {
          const Component = applicationPages[name];
          return <Component {...props} />;
        }}
      />
    );
  }

  renderRoutes(routes: IRoute[]) {
    return routes.map(this.renderRoute);
  }

  // RENDER

  render() {
    const { pristine } = this.props;

    return (
      <>
        <Loader scale={4} loaded={!pristine}>
          <Switch>
            {this.renderRoutes(sessionRoutes)}
            <AuthenticationAssurance>
              <Switch>
                {this.renderRoutes(applicationRoutes)}
                <Route render={() => <h1>404</h1>} />
              </Switch>
            </AuthenticationAssurance>
          </Switch>
        </Loader>
        <ToastContainer />
      </>
    );
  }
}

// EXPORT

export const Pages = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PagesComponent);
