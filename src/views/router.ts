import { routerWith } from "utils/routes";

import { router as dashboardRouter, routes as dashboardRoutes } from "views/Dashboards/router";
import { router as devicesRouter, routes as devicesRoutes } from "views/Devices/router";
import { router as homeRouter, routes as homeRoutes } from "views/Home/router";
import { router as infoRouter, routes as infoRoutes } from "views/Info/router";
import { router as sessionRouter, routes as sessionRoutes } from "views/Session/router";

export const routes = [...dashboardRoutes, ...devicesRoutes, ...homeRoutes, ...sessionRoutes, ...infoRoutes];
export const router = routerWith([dashboardRouter, devicesRouter, homeRouter, sessionRouter, infoRouter]);
