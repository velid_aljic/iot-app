import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Button, Header, Icon, Modal } from "semantic-ui-react";

import { deleteDevice, EDevice, IRemoveDevice } from "data/devices";

import { IRootStore } from "store";

/**
 *
 *
 * INTERFACE
 */

// PROPS

interface DispatchProps {
  deleteDevice: (params: IRemoveDevice) => Promise<EDevice>;
}

interface StateProps {
  isWorking: boolean;
}

interface MatchParams {
  id: string;
}

interface ComponentProps extends RouteComponentProps<MatchParams> {
  id: number;
  toggle: (e: any) => void;
}

interface Props extends DispatchProps, StateProps, ComponentProps {}

/**
 *
 *
 * MAPPING
 */

const mapStateToProps = (state: IRootStore) => {
  const { isWorking } = state.devices;

  return {
    isWorking,
  };
};

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    deleteDevice: deleteDevice(dispatch),
  };
};

/**
 *
 *
 * CLASS
 */

export class DeleteDeviceComponent extends React.PureComponent<Props> {
  // COMPONENT METHODS

  remove = async () => {
    const { id } = this.props;

    try {
      await this.props.deleteDevice({ id });
      this.props.history.replace("/devices");
    } catch (response) {
      // @todo: add error handler
    }
  };

  // RENDER

  render() {
    const { isWorking, toggle } = this.props;

    return (
      <Modal open closeIcon dimmer="blurring" onClose={toggle} closeOnDimmerClick={false}>
        <Header icon="block layout" content="Delete Device" />
        <>
          <Modal.Content>
            <p>Are you sure you want to delete selected device?</p>
          </Modal.Content>
        </>
        <>
          <Modal.Actions>
            <Button icon onClick={toggle}>
              <Icon name="remove" size="big" />
            </Button>
            <Button icon loading={isWorking} onClick={this.remove}>
              <Icon name="trash" size="big" />
            </Button>
          </Modal.Actions>
        </>
      </Modal>
    );
  }
}

/**
 *
 *
 * EXPORT
 */

export const DeleteDeviceModal = withRouter(
  connect<StateProps, DispatchProps, ComponentProps>(
    mapStateToProps,
    mapDispatchToProps,
  )(DeleteDeviceComponent),
);
