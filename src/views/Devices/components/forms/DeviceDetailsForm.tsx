import React from "react";
import CopyToClipboard from "react-copy-to-clipboard";
import { Button, Confirm, Divider, Form, Header, Icon, Input, Label } from "semantic-ui-react";

import { IDevice, IDeviceSecret, IRemoveDevice } from "data/devices";

import { SemanticICONS } from "semantic-ui-react/dist/commonjs/generic";
import "./_device_details_form.scss";

interface Props {
  device: IDevice;
  isFetchingSecret: boolean;
  secret: IDeviceSecret;
  remove: (params: IRemoveDevice) => void;
}

interface State {
  openDelete: boolean;
  hasCopiedId: boolean;
  hasCopiedSecret: boolean;
}

const copiedLabel = (
  <Label basic pointing="left">
    Copied!
  </Label>
);

export class DeviceDetailsForm extends React.Component<Props, State> {
  state = { hasCopiedId: false, hasCopiedSecret: false, openDelete: false };

  toggleDeleteModal = () => {
    this.setState({ openDelete: !this.state.openDelete });
  };

  copyId = () => {
    if (!this.state.hasCopiedId) {
      this.setState({ hasCopiedId: true, hasCopiedSecret: false }, () =>
        setTimeout(() => {
          this.setState({ hasCopiedId: false });
        }, 1000),
      );
    }
  };

  copySecret = () => {
    if (!this.state.hasCopiedSecret) {
      this.setState({ hasCopiedId: false, hasCopiedSecret: true }, () =>
        setTimeout(() => {
          this.setState({ hasCopiedSecret: false });
        }, 1000),
      );
    }
  };

  confirmModalButton = (icon: string) => {
    return (
      <Button icon>
        <Icon name={icon as SemanticICONS} size="large" />
      </Button>
    );
  };

  render() {
    const { isFetchingSecret, secret, device, remove } = this.props;
    const { hasCopiedId, hasCopiedSecret, openDelete } = this.state;
    return (
      <>
        <Confirm
          header="Delete Device"
          content="Are you sure you want to delete selected device?"
          open={openDelete}
          onCancel={this.toggleDeleteModal}
          onConfirm={() => remove({ id: device.id })}
          cancelButton={this.confirmModalButton("remove")}
          confirmButton={this.confirmModalButton("trash")}
        />

        <Header content="Device Details" icon="info circle" />

        <Divider />

        <Form>
          <Form.Field className="secretField" width={16}>
            <label style={{}}>Device Name</label>
            <Input disabled={true} value={device.name} />
          </Form.Field>

          <Form.Field>
            <label style={{}}>Device Id</label>
            <Form.Group inline>
              <Form.Field className="secretField" width={11}>
                <Input disabled={true} value={device.id} />
              </Form.Field>
              <Form.Field width={4} inline>
                <CopyToClipboard text={device.id.toString()} onCopy={this.copyId}>
                  <Button icon="copy" />
                </CopyToClipboard>
                {hasCopiedId && copiedLabel}
              </Form.Field>
            </Form.Group>
          </Form.Field>

          <Form.Field>
            <label style={{}}>Device Secret</label>
            <Form.Group inline>
              <Form.Field className="secretField" width={11}>
                <Input disabled={true} loading={isFetchingSecret} value={secret.deviceSecret} />
              </Form.Field>
              <Form.Field width={4}>
                <CopyToClipboard text={secret.deviceSecret} onCopy={this.copySecret}>
                  <Button icon="copy" />
                </CopyToClipboard>
                {hasCopiedSecret && copiedLabel}
              </Form.Field>
            </Form.Group>
          </Form.Field>

          <Form.Field className="secretField" width={14}>
            <label style={{}}>Secret Algorithm</label>
            <Input disabled={true} loading={isFetchingSecret} value={secret.secretAlgorithm} />
          </Form.Field>

          <Divider />

          <Form.Group>
            <Button floated="right" icon onClick={this.toggleDeleteModal}>
              <Icon name="trash" size="large" />
            </Button>
          </Form.Group>
        </Form>
      </>
    );
  }
}
