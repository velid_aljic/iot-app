import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Field, reduxForm } from "redux-form";
import { Button, Divider, Form, Header, Icon } from "semantic-ui-react";

import { Dropdown, Input, required } from "components/Form";

import { createDevice, deviceTypes, EDevice, IPostDevice } from "data/devices";

import { IRootStore } from "store";

/**
 *
 *
 * CONSTANTS
 */

export const CREATE_DEVICE_FORM = "createDeviceForm";

/**
 *
 *
 * INTERFACE
 */

export interface ICreateDeviceForm {
  name: string;
  deviceType: number;
}

/**
 *
 *
 * FORM
 */

// @TODO: ADD PROP TYPES
const CreateDeviceForm = (props: any) => {
  const { handleSubmit, reset, pristine, submitting } = props;

  return (
    <>
      <Header content="Create New Device" icon="block layout" />

      <Divider />

      <Form onSubmit={handleSubmit}>
        <Form.Group>
          <Field
            name="name"
            label="Device name"
            type="text"
            placeholder="eg. My Device"
            width={16}
            required={true}
            validate={[required]}
            component={Input}
          />
        </Form.Group>
        <Form.Group>
          <Field
            name="deviceType"
            label="Device type"
            placeholder="eg. Thermometer"
            width={16}
            validate={[required]}
            required={true}
            component={Dropdown}
            options={deviceTypes}
          />
        </Form.Group>

        <Divider />

        <Form.Group>
          <Button floated="right" icon disabled={pristine || submitting} onClick={reset}>
            <Icon name="refresh" size="large" />
          </Button>
          <Button floated="right" icon disabled={pristine || submitting}>
            <Icon name="save" size="large" />
          </Button>
        </Form.Group>
      </Form>
    </>
  );
};

/**
 *
 *
 * CONNECTED FORM
 */

const CreateDeviceReduxForm = reduxForm({
  form: CREATE_DEVICE_FORM,
})(CreateDeviceForm);

/**
 *
 *
 * CONNECTED COMPONENT
 */

// PROPS

interface DispatchProps {
  createDevice: (data: IPostDevice) => Promise<EDevice>;
}

interface StateProps {
  isWorking: boolean;
}

interface ComponentProps extends RouteComponentProps {
  toggle: () => void;
}

interface Props extends DispatchProps, StateProps, ComponentProps {}

/**
 *
 *
 * MAPPING
 */

const mapStateToProps = (state: IRootStore) => {
  const { isWorking } = state.devices;

  return {
    isWorking,
  };
};

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    createDevice: createDevice(dispatch),
  };
};

/**
 *
 *
 * CLASS
 */

export class CreateDeviceComponent extends React.PureComponent<Props> {
  // COMPONENT METHODS

  save = async (values: ICreateDeviceForm) => {
    const { name, deviceType } = values;

    try {
      await this.props.createDevice({ name, deviceTypeId: deviceType });
      /* const device = await this.props.createDevice({ name, deviceTypeId: deviceType });
      this.props.history.push(`/devices/${device.id}`); */
    } catch (response) {
      // @todo: add error handler
    } finally {
      this.props.toggle();
    }
  };

  model = ({ name, device_type }: any) => ({
    name,
    device_type_id: device_type,
  });

  // RENDER

  render() {
    return <CreateDeviceReduxForm onSubmit={this.save} />;
  }
}

/**
 *
 *
 * EXPORT
 */

export const CreateDeviceConnectedForm = withRouter(
  connect<StateProps, DispatchProps, ComponentProps>(
    mapStateToProps,
    mapDispatchToProps,
  )(CreateDeviceComponent),
);
