import { SemanticICONS } from "semantic-ui-react";

import { routerFrom } from "utils/routes";

export const routes = [
  {
    rank: 2,
    exact: true,
    inSidebar: true,
    label: "Devices",
    name: "devices-list",
    icon: "block layout" as SemanticICONS,
    path: "/devices",
  },
  // {
  //   exact: true,
  //   label: "Device Details",
  //   name: "device-details",
  //   path: "/devices/:id",
  // },
];

export const router = routerFrom(routes);
