import moment from "moment";
import React from "react";
import Loader from "react-loader";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Breadcrumb, Button, Card, Grid, Icon, Menu, Pagination, Popup, Search } from "semantic-ui-react";

import { Pushable } from "components/Pushable";

import {
  deleteDevice,
  EDevice,
  EDevices,
  EDeviceSecret,
  fetchDevices,
  fetchDeviceSecret,
  IDevice,
  IDevices,
  IDeviceSecret,
  IGetDeviceSecret,
  IRemoveDevice,
  resolveIcon,
} from "data/devices";

import { ContextInfo, ContextMenu } from "views/Session/components/ContextPortal";

import { IRootStore } from "store";

import { CreateDeviceConnectedForm, DeviceDetailsForm } from "../components/forms";

/**
 *
 *
 * INTERFACES
 */

// PROPS

interface StateProps {
  devices: IDevices;
  isFetchingSecret: boolean;
  isWorking: boolean;
  secret: IDeviceSecret;
}

interface DispatchProps {
  deleteDevice: (params: IRemoveDevice) => Promise<EDevice>;
  fetchDevices: () => Promise<EDevices>;
  fetchDeviceSecret: (params: IGetDeviceSecret) => Promise<EDeviceSecret>;
}

interface ComponentProps extends RouteComponentProps {}

interface Props extends ComponentProps, DispatchProps, StateProps {}

interface State {
  currentDeviceId?: number;
  openCreate: boolean;
  openDetails: boolean;
}

/**
 *
 *
 * MAPPING
 */

const mapStateToProps = (state: IRootStore) => {
  const { list, isWorking, isFetchingSecret, secret } = state.devices;

  return {
    devices: list,
    isFetchingSecret,
    isWorking,
    secret,
  };
};

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    deleteDevice: deleteDevice(dispatch),
    fetchDevices: fetchDevices(dispatch),
    fetchDeviceSecret: fetchDeviceSecret(dispatch),
  };
};

/**
 *
 *
 * CLASS
 */

class DevicesListComponent extends React.PureComponent<Props, State> {
  state = { currentDeviceId: undefined, openCreate: false, openDetails: false };

  /**
   *
   *
   * LIFECYCLE METHODS
   */

  componentDidMount() {
    this.fetchData();
  }

  /**
   *
   *
   * COMPONENT METHODS
   */

  fetchData() {
    this.props.fetchDevices();
  }

  forward(path: string) {
    this.props.history.push(path);
  }

  handleClick = (deviceId: number) => {
    const path = `/devices/${deviceId}`;
    this.forward(path);
  };

  removeDevice = (params: IRemoveDevice) => {
    this.setState({ openDetails: false }, () => {
      return this.props.deleteDevice(params);
    });
  };

  getDeviceById = (id: number): IDevice => {
    return this.props.devices.find((device: IDevice) => {
      return device.id === id;
    }) as IDevice;
  };

  toggleCreateDevice = () => {
    const { openCreate, openDetails } = this.state;
    return openDetails && !openCreate
      ? this.setState({ openDetails: false })
      : this.setState({ openCreate: !this.state.openCreate, openDetails: false });
  };

  toggleDetailsDevice = (deviceId: number) => {
    this.setState({ openDetails: !this.state.openDetails, openCreate: false, currentDeviceId: deviceId }, () => {
      this.props.fetchDeviceSecret({ deviceId });
    });
  };

  /**
   *
   *
   * RENDER METHODS
   */

  renderInfo = () => {
    return (
      <Menu.Item>
        <Breadcrumb size="big">
          <Breadcrumb.Section active>Devices</Breadcrumb.Section>
        </Breadcrumb>
      </Menu.Item>
    );
  };

  renderMenu = () => {
    const { openCreate, openDetails } = this.state;

    const create = (
      <Button circular icon={openCreate || openDetails ? "close" : "add"} onClick={this.toggleCreateDevice} />
    );
    const filter = <Button circular icon="filter" />;

    return (
      <>
        <Menu.Item className="menuSearch">
          <Search aligned="right" size="mini" placeholder="Search..." />
        </Menu.Item>

        <Menu.Item>
          <Popup
            position="bottom right"
            content={openCreate || openDetails ? "Close" : "Create device"}
            trigger={create}
          />
        </Menu.Item>
        <Menu.Item>
          <Popup content="Filter" position="bottom right" trigger={filter} />
        </Menu.Item>
      </>
    );
  };

  renderDeviceCard = (device: IDevice) => {
    const { id, deviceType, name, modifiedAt, createdAt } = device;

    return (
      <Grid.Column stretched key={`device_${id}`} textAlign="center" width={4}>
        <Card fluid link onClick={() => this.toggleDetailsDevice(id)}>
          <Card.Content>
            <Icon name={resolveIcon(deviceType.id)} size="huge" color="black" />
          </Card.Content>
          <Card.Content>
            <Card.Description>{name ? name : "Untitled device"}</Card.Description>
            <Card.Meta>{deviceType.name}</Card.Meta>
            <Card.Meta>Created at: {moment(createdAt).format("DD.MM.YYYY hh:mm")}</Card.Meta>
            <Card.Meta>Modified at: {moment(modifiedAt).format("DD.MM.YYYY hh:mm")}</Card.Meta>
          </Card.Content>
          <Card.Content extra>
            <Icon name="user" />1 User
          </Card.Content>
        </Card>
      </Grid.Column>
    );
  };

  /**
   *
   *
   * RENDER
   */

  render() {
    const { openCreate, openDetails, currentDeviceId } = this.state;
    const { devices, isWorking, isFetchingSecret, secret } = this.props;

    const forms = (
      <>
        {openCreate && <CreateDeviceConnectedForm toggle={this.toggleCreateDevice} />}
        {openDetails && (
          <DeviceDetailsForm
            device={this.getDeviceById(currentDeviceId || 0)}
            isFetchingSecret={isFetchingSecret}
            remove={this.removeDevice}
            secret={secret}
          />
        )}
      </>
    );

    return (
      <>
        <ContextInfo loaded={!isWorking}>{this.renderInfo()}</ContextInfo>
        <ContextMenu loaded={!isWorking}>{this.renderMenu()}</ContextMenu>

        <Pushable open={openCreate || openDetails} forms={forms}>
          <Loader scale={2} loaded={!isWorking}>
            <Grid centered padded stackable doubling columns={4}>
              {devices.map(this.renderDeviceCard)}
            </Grid>
            <Grid centered>
              <Pagination defaultActivePage={1} firstItem={null} lastItem={null} pointing secondary totalPages={1} />
            </Grid>
          </Loader>
        </Pushable>
      </>
    );
  }
}

/**
 *
 *
 * EXPORT
 */

export const DevicesListPage = withRouter(
  connect<StateProps, DispatchProps, ComponentProps>(
    mapStateToProps,
    mapDispatchToProps,
  )(DevicesListComponent),
);
