import { withNavigationLayout } from "views/Session/components/NavigationLayout";

// import { DeviceDetailsPage } from "views/Devices/Detail";
import { DevicesListPage } from "views/Devices/List";

export const pages = {
  "devices-list": withNavigationLayout(DevicesListPage),
  // "device-details": withNavigationLayout(DeviceDetailsPage),
};
