import React, { ReactNode } from "react";
import Loader from "react-loader";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Breadcrumb, Button, Card, Grid, Menu, Popup } from "semantic-ui-react";

import ErrorBus from "components/Application/Errors";

import { EDevice, fetchDevice, IDevice, IGetDevice } from "data/devices";

import { ContextInfo, ContextMenu } from "views/Session/components/ContextPortal";

import { IRootStore } from "store";

import { DeleteDeviceModal } from "../components/modals";

/**
 *
 *
 * INTERFACES
 */

// PROPS

interface StateProps {
  device: IDevice;
  isWorking: boolean;
}

interface DispatchProps {
  fetchDevice: (params: IGetDevice) => Promise<EDevice>;
}

interface MatchParams {
  id: string;
}

interface ComponentProps extends RouteComponentProps<MatchParams> {}

interface Props extends ComponentProps, DispatchProps, StateProps {}

// STATE

interface State {
  isOpenCreateModal: boolean;
  isOpenDeleteModal: boolean;
}

/**
 *
 *
 * MAPING
 */

const mapStateToProps = (state: IRootStore) => {
  const { item, isWorking } = state.devices;

  return {
    device: item,
    isWorking,
  };
};

const mapDispatchToProps = (dispatch: Dispatch, getState: any) => {
  return {
    fetchDevice: fetchDevice(dispatch),
  };
};

/**
 *
 *
 * CLASS
 */

class DeviceDetailsComponent extends React.PureComponent<Props, State> {
  state = { isOpenCreateModal: false, isOpenDeleteModal: false };

  /**
   *
   *
   * LIFECYCLE METHODS
   */

  componentDidMount() {
    const deviceId = Number(this.props.match.params.id);

    if (Number.isNaN(deviceId)) {
      const ERR_MSG = "Invalid parameter provided";
      return ErrorBus.publish("APP/UNCAUGHT", new Error(ERR_MSG));
    }

    this.props.fetchDevice({ id: deviceId });
  }

  /**
   *
   *
   * COMPONENT METHODS
   */

  toggleDeleteModal = () => {
    this.setState({ isOpenDeleteModal: !this.state.isOpenDeleteModal });
  };

  toggleCreateModal = () => {
    this.setState({ isOpenCreateModal: !this.state.isOpenCreateModal });
  };

  /**
   *
   *
   * RENDER METHODS
   */

  renderInfo = () => {
    const { name } = this.props.device;

    return (
      <Menu.Item>
        <Breadcrumb size="big">
          <Breadcrumb.Section link onClick={() => this.props.history.push("/devices")}>
            Devices
          </Breadcrumb.Section>
          <Breadcrumb.Divider icon="right angle" />
          <Breadcrumb.Section active>{name}</Breadcrumb.Section>
        </Breadcrumb>
      </Menu.Item>
    );
  };

  renderMenu = () => {
    const create = <Button disabled circular icon="add" onClick={this.toggleCreateModal} />;
    const remove = <Button circular icon="trash" onClick={this.toggleDeleteModal} />;

    return (
      <>
        <Menu.Item>
          <Popup content="Add widget" position="bottom right" trigger={create} />
        </Menu.Item>
        <Menu.Item>
          <Popup content="Delete device" position="bottom right" trigger={remove} />
        </Menu.Item>
      </>
    );
  };

  renderCardWithProps = (children: ReactNode) => {
    return (
      <Card raised fluid>
        <Card.Content>{children}</Card.Content>
      </Card>
    );
  };

  /**
   *
   *
   * RENDER
   */

  render() {
    const { isOpenDeleteModal } = this.state;
    const { device, isWorking } = this.props;

    return (
      <>
        {isOpenDeleteModal && <DeleteDeviceModal id={device.id} toggle={this.toggleDeleteModal} />}

        <ContextInfo loaded={!isWorking}>{this.renderInfo()}</ContextInfo>
        <ContextMenu loaded={!isWorking}>{this.renderMenu()}</ContextMenu>

        <Loader scale={2} loaded={!isWorking}>
          <Grid centered padded />
        </Loader>
      </>
    );
  }
}

/**
 *
 *
 * EXPORT
 */

export const DeviceDetailsPage = withRouter(
  connect<StateProps, DispatchProps, ComponentProps>(
    mapStateToProps,
    mapDispatchToProps,
  )(DeviceDetailsComponent),
);
