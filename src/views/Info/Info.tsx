import React from "react";
import { Breadcrumb, Container, Menu, Message } from "semantic-ui-react";

import { ContextInfo } from "views/Session/components/ContextPortal";

export class InfoPage extends React.Component {
  state = { loaded: false };

  componentDidMount() {
    this.setState({ loaded: true });
  }

  /**
   *
   *
   * RENDER METHODS
   */
  // tslint: disable
  renderInfo = () => {
    return (
      <Menu.Item>
        <Breadcrumb size="big">
          <Breadcrumb.Section active>Info</Breadcrumb.Section>
        </Breadcrumb>
      </Menu.Item>
    );
  };

  render() {
    return (
      <>
        <ContextInfo loaded={this.state.loaded}>{this.renderInfo()}</ContextInfo>
        <Container>
          
          <Message
            size="small"
            header="Application info"
            content={
              <pre>
                Version: 0.0.1
                <br/>
                Frontend repository: <a href="https://bitbucket.org/velid_aljic/iot-app">https://bitbucket.org/velid_aljic/iot-app</a>, Velid Aljic
                <br/>
                Server repository: <a href="https://bitbucket.org/smarthomiessarajevo/backend/">https://bitbucket.org/smarthomiessarajevo/backend/</a>, Ensar Sarajcic
              </pre>
            }
          />

          <Message
            size="small"
            header="MQTT configuration"
            content={
              <pre>
                <code>
                  {`
                        "broker": {
                          "url": "broker.hivemq.com",
                          "port": "1883"
                        },
                        "endpoints": [
                          {
                            "body_example": {
                              "record_type": 1,
                              "record_value": 123,
                              "recorded_at": 1537379424,
                              "hmac": "jfdhslfh12383j12l3j12oirjfkdsfd"
                            },
                            "description": "Used by devices to send data to server. 
                            All messages sent to this endpoint must be in JSON format and signed 
                            with device secret (signature should be added to original JSON after signing as \"hmac\" key in root object). 
                            JSON can contain any number of keys, but there are a few mandatory fields.",
                            "topic": "device/<device_id>"
                          },
                          {
                            "body_example": {
                              "update_rate": 4,
                              "mode": "passive"
                            },
                            "description": "Used by server to send config to devices. 
                            Devices should listen to this endpoint in order to properly receive updated configuration.",
                            "topic": "device/<device_id>/config"
                          }
                        ]
                  `}
                </code>
              </pre>
            }

          />

          <Message
            error
            size="small"
            header="Currently in development"
            content={
              <pre>
                - Fix account registration workflow
                <br/>
                - Fix token expiration issue
                <br/>
                - Fix "404 - not found" page layout
                <br/>
                - Add more charts/visuals in dashboard workspace
                <br/>
                - Add "edit-profile" page
                <br/>
                - Add search devices/dashboards functionality
                <br/>
                - Add pagination
              </pre>
            }
          />
        </Container>
      </>
    );
  }
}
