import { SemanticICONS } from "semantic-ui-react";

import { routerFrom } from "utils/routes";

export const routes = [
  {
    rank: 3,
    exact: true,
    inSidebar: true,
    label: "Info",
    name: "info",
    icon: "info circle" as SemanticICONS,
    path: "/info",
  },
];

export const router = routerFrom(routes);
