import { withNavigationLayout } from "views/Session/components/NavigationLayout";

import { InfoPage } from "./Info";

export const pages = {
  info: withNavigationLayout(InfoPage),
};
