import { pages as dashboardPages } from "views/Dashboards/views";
import { pages as devicesPages } from "views/Devices/views";
import { pages as homePages } from "views/Home/views";
import { pages as infoPages } from "views/Info/views";
import { pages as sessionPages } from "views/Session/views";

export const pages: { [key: string]: any } = {
  ...dashboardPages,
  ...devicesPages,
  ...homePages,
  ...sessionPages,
  ...infoPages,
};
