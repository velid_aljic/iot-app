import React, { SyntheticEvent } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Button, Form, Grid, Header, Icon, Message, Segment } from "semantic-ui-react";

import { asyncLogin, ICredentials } from "data/session";

import { IRootStore } from "store";

const HOME_PATH = "/";
const SIGNUP_PATH = "/signup";

// INTERFACE

interface Props extends RouteComponentProps {
  authenticated: boolean;
  isWorking: boolean;
  login: (data: ICredentials) => void;
}

// MAPING

const mapStateToProps = (state: IRootStore) => {
  const { authenticated, isWorking } = state.session;

  return {
    authenticated,
    isWorking,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    login: asyncLogin(dispatch),
  };
};

// CLASS

class LoginComponent extends React.PureComponent<Props> {
  state = { username: "", password: "" };

  // LIFECYCLE METHODS

  componentDidMount() {
    if (this.props.authenticated) {
      this.redirect();
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.authenticated && !prevProps.authenticated) {
      this.redirect();
    }
  }

  // COMPONENT METHODS

  redirect() {
    this.props.history.replace(HOME_PATH);
  }

  redirectSignup = () => {
    this.props.history.replace(SIGNUP_PATH);
  };

  handleSubmit = () => {
    const { username, password } = this.state;
    this.props.login({ username, password });
  };

  handleChange = (e: SyntheticEvent<HTMLInputElement>, { name, value }: any) => {
    this.setState({ [name]: value });
  };

  // RENDER METHODS

  renderFormInput = (value: string, name: string, type: string, icon: string) => {
    return (
      <Form.Input
        name={name}
        value={value}
        onChange={this.handleChange}
        fluid={true}
        icon={icon}
        iconPosition="left"
        placeholder={name}
        type={type}
      />
    );
  };

  // RENDER

  render() {
    const { username, password } = this.state;
    const { isWorking } = this.props;

    return (
      <Grid textAlign="center" style={{ height: "100%", marginTop: "-5%" }} verticalAlign="middle">
        <Grid.Column style={{ maxWidth: 450 }}>
          <Icon name="code" size="huge" />
          <Header as="h2" textAlign="center" content="Log-in to your account" />
          <Form size="large" onSubmit={this.handleSubmit}>
            <Segment stacked={true}>
              {this.renderFormInput(username, "username", "input", "user")}
              {this.renderFormInput(password, "password", "password", "lock")}
              <Button
                content="Login"
                size="large"
                color="black"
                fluid={true}
                loading={isWorking}
                disabled={isWorking}
              />
            </Segment>
          </Form>
          <Message>
            New to us?{" "}
            <a style={{ cursor: "pointer" }} onClick={this.redirectSignup}>
              Sign Up
            </a>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

// EXPORT

export const LoginPage = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(LoginComponent),
);
