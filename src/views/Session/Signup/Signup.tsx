import React, { SyntheticEvent } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";
import { Button, Form, Grid, Header, Icon, Message, Segment } from "semantic-ui-react";

import { asyncSignup, ISignup } from "data/session";

import { IRootStore } from "store";

const HOME_PATH = "/home";
const LOGIN_PATH = "/login";

// INTERFACE

interface Props extends RouteComponentProps {
  authenticated: boolean;
  isWorking: boolean;
  signup: (data: ISignup) => void;
}

// MAPING

const mapStateToProps = (state: IRootStore) => {
  const { authenticated, isWorking } = state.session;

  return {
    authenticated,
    isWorking,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    signup: asyncSignup(dispatch),
  };
};

// CLASS

class SignupComponent extends React.PureComponent<Props> {
  state = { email: "", username: "", password: "" };

  // LIFECYCLE METHODS

  componentDidMount() {
    if (this.props.authenticated) {
      this.redirect();
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.authenticated && !prevProps.authenticated) {
      this.redirect();
    }
  }

  // COMPONENT METHODS

  redirect() {
    this.props.history.replace(HOME_PATH);
  }

  redirectLogin = () => {
    this.props.history.replace(LOGIN_PATH);
  };

  handleSubmit = () => {
    const { email, username, password } = this.state;
    this.props.signup({ email, username, password });
  };

  handleChange = (e: SyntheticEvent<HTMLInputElement>, { name, value }: any) => {
    this.setState({ [name]: value });
  };

  // RENDER METHODS

  renderFormInput = (value: string, name: string, type: string, icon: string) => {
    return (
      <Form.Input
        name={name}
        value={value}
        onChange={this.handleChange}
        fluid={true}
        icon={icon}
        iconPosition="left"
        placeholder={name}
        type={type}
      />
    );
  };

  // RENDER

  render() {
    const { email, username, password } = this.state;
    const { isWorking } = this.props;

    return (
      <Grid textAlign="center" style={{ height: "100%", marginTop: "-5%" }} verticalAlign="middle">
        <Grid.Column style={{ maxWidth: 450 }}>
          <Icon name="code" size="huge" />
          <Header as="h2" textAlign="center" content="Sign up to iot-app" />
          <Form size="large" onSubmit={this.handleSubmit}>
            <Segment stacked={true}>
              {this.renderFormInput(email, "email", "input", "mail")}
              {this.renderFormInput(username, "username", "input", "user")}
              {this.renderFormInput(password, "password", "password", "lock")}
              <Button
                content="Sign Up"
                size="large"
                color="black"
                fluid={true}
                loading={isWorking}
                disabled={isWorking}
              />
            </Segment>
          </Form>
          <Message>
            Already have an account?{" "}
            <a style={{ cursor: "pointer" }} onClick={this.redirectLogin}>
              Log in
            </a>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

// EXPORT

export const SignupPage = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(SignupComponent),
);
