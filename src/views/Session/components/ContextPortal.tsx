import { isNull } from "lodash";
import React from "react";
import ReactDOM from "react-dom";
import { Button, Menu } from "semantic-ui-react";

import { TOPBAR_CONTEXT_INFO, TOPBAR_CONTEXT_MENU } from "./NavigationLayout";

// INTERFACES

interface Props {
  loaded: boolean;
}

// CLASS

const ContextHoc = (target: string) => {
  return class ContextPortal extends React.PureComponent<Props> {
    // RENDER METHODS

    loader = () => {
      return (
        <Menu.Item>
          <Button circular loading icon="circle" />
        </Menu.Item>
      );
    };

    // RENDER

    render() {
      const topbarMenu = document.getElementById(target);
      const { children, loaded } = this.props;

      if (isNull(topbarMenu)) {
        return null;
      }

      const elem = loaded ? children : this.loader();

      return ReactDOM.createPortal(elem, topbarMenu);
    }
  };
};

export const ContextMenu = ContextHoc(TOPBAR_CONTEXT_MENU);
export const ContextInfo = ContextHoc(TOPBAR_CONTEXT_INFO);
