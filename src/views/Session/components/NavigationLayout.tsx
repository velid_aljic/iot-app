import React, { ComponentClass, ReactNode } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Dispatch } from "redux";

import { Navigation } from "components/Navigation";

import { asyncLogout } from "data/session";

import { filterSidebar, IRoute, rankSorter } from "utils/routes";

import { routes as applicationRoutes } from "views/router";

// tslint:disable-next-line
const logo = require("./logo.png");

// CONSTANTS

export const TOPBAR_CONTEXT_MENU = "topbar-context-menu";
export const TOPBAR_CONTEXT_INFO = "topbar-context-info";

// INTERFACES

interface Props extends RouteComponentProps {
  children: ReactNode;
  logout: () => void;
}

interface State {}

// MAPPING

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    logout: asyncLogout(dispatch),
  };
};

// CLASS

class NavLayoutComponent extends React.PureComponent<Props, State> {
  // COMPONENT METHODS

  forward = (path: string) => this.props.history.push(path);
  logout = () => this.props.logout();

  trimRoutes = (routes: IRoute[]) => {
    return routes.filter(filterSidebar).sort(rankSorter);
  };

  // RENDER

  render() {
    const { children } = this.props;
    const sidebarRoutes = this.trimRoutes(applicationRoutes);

    return (
      <Navigation
        logo={logo}
        children={children}
        sidebarRoutes={sidebarRoutes}
        contextMenu={TOPBAR_CONTEXT_MENU}
        contextInfo={TOPBAR_CONTEXT_INFO}
        logout={this.logout}
        forward={this.forward}
      />
    );
  }
}

const NavLayout = withRouter(
  connect(
    null,
    mapDispatchToProps,
  )(NavLayoutComponent),
);

// EXPORT

export const withNavigationLayout = (children: ComponentClass) => {
  return () => <NavLayout>{React.createElement(children)}</NavLayout>;
};
