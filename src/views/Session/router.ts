import { routerFrom } from "utils/routes";

export const routes = [
  {
    exact: true,
    label: "Login",
    name: "login",
    path: "/login",
  },
  {
    exact: true,
    label: "Signup",
    name: "signup",
    path: "/signup",
  },
];

export const router = routerFrom(routes);
