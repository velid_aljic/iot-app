import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { Card, Grid, Header, Icon } from "semantic-ui-react";

import { filterHome, IRoute, rankSorter } from "utils/routes";

import { routes as applicationRoutes } from "views/router";

/**
 *
 *
 * INTERFACES
 */

interface Props extends RouteComponentProps {}

/**
 *
 *
 * CLASS
 */

class HomeComponent extends React.PureComponent<Props> {
  // COMPONENT METHODS

  handleClick = (path: string) => this.props.history.push(path);

  // RENDER METHODS

  renderCards = (routes: IRoute[]) => {
    return routes
      .filter(filterHome)
      .sort(rankSorter)
      .map(this.renderCard);
  };

  renderCard = (route: IRoute) => {
    return (
      <Grid.Column stretched key={`home_${route.label}`} textAlign="center" width={3}>
        <Card fluid link onClick={() => this.handleClick(route.path)}>
          <Card.Content>
            <Icon name={route.icon} size="huge" color="black" />
            <Header as="h3">{route.label}</Header>
          </Card.Content>
        </Card>
      </Grid.Column>
    );
  };

  // RENDER

  render() {
    return (
      <>
        <Grid columns={3} centered padded stackable doubling>
          {this.renderCards(applicationRoutes)}
        </Grid>
      </>
    );
  }
}

/**
 *
 *
 * EXPORT
 */

export const HomePage = withRouter(HomeComponent);
