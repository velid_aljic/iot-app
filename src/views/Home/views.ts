import { withNavigationLayout } from "views/Session/components/NavigationLayout";

import { HomePage } from "./HomePage";

export const pages = {
  home: withNavigationLayout(HomePage),
};
