import { SemanticICONS } from "semantic-ui-react";

import { routerFrom } from "utils/routes";

export const routes = [
  {
    rank: 0,
    exact: true,
    inSidebar: true,
    label: "Home",
    name: "home",
    icon: "home" as SemanticICONS,
    path: "/",
  },
];

export const router = routerFrom(routes);
