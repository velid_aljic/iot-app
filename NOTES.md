/\*
@note /data/devices/store.ts
@todo: Move paragraph to another file.

Issue when importing eg. fetchDevices method from services:
If it is imported from ./service, gets assigned as parameter to a function, and then that function gets exported and used in another file, everything goes as expected.
But if it gets imported through data/devices, assigned as parameter to another function, and then that function gets exported and used in another file, parameter
to whom fetchDevices method was assigned will be undefined.
But also, when exporting object as function everthing works ok.
@note: When importing something, import from the shortest possible path!
\*/
